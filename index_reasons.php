<section class="features">
    <div class="container">
        <div class="features__inner">
            <h2 class="h2 features__title">Зачем подключаться к маркетплейсам</h2>
            <div class="features__block-list">
                <div class="features__block-item">
                    <div class="features__block-top">
                        <img class="features__block-img" src="<?= DIST_SITE_DIR_PATH; ?>img/features/icon-1.svg" alt="Готовая витрина товаров">
                    </div>
                    <div class="features__block-text">Готовая витрина товаров</div>
                </div>
                <div class="features__block-item">
                    <div class="features__block-top">
                        <img class="features__block-img" src="<?= DIST_SITE_DIR_PATH; ?>img/features/icon-2.svg" alt="Дополнительный канал продаж">
                    </div>
                    <div class="features__block-text">Дополнительный канал продаж</div>
                </div>
                <div class="features__block-item">
                    <div class="features__block-top">
                        <img class="features__block-img" src="<?= DIST_SITE_DIR_PATH; ?>img/features/icon-3.svg" alt="SEO на аутсорсе">
                    </div>
                    <div class="features__block-text">Самый растущий и перспективный сегмент ecommerce</div>
                </div>
                <div class="features__block-item">
                    <div class="features__block-top">
                        <img class="features__block-img" src="<?= DIST_SITE_DIR_PATH; ?>img/features/icon-4.svg" alt="Фискализация под ключ">
                    </div>
                    <div class="features__block-text">Выход на несколько релевантных площадок</div>
                </div>
                <div class="features__block-item">
                    <div class="features__block-top">
                        <img class="features__block-img" src="<?= DIST_SITE_DIR_PATH; ?>img/features/icon-5.svg" alt="Универсализация приема платежей">
                    </div>
                    <div class="features__block-text">Возможность конкурировать с сильными игроками</div>
                </div>
            </div>
        </div>
    </div>
</section>