<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Letsrock\Lib\Models\Helper;
use Letsrock\Lib\Models\PropertyHL;
use \CIBlockElement;
CModule::IncludeModule('iblock');

$capabilities = PropertyHL::getHlProperties(HL_CAPABILITIES_ID);
$elementRes = CIBlockElement::GetlIst([], ['IBLOCK_ID' => IB_CAPABILITIES_IBLOCK_ID, 'ID' => IB_CAPABILITIES_ID]);
?>

<? if ($element = $elementRes->GetNext()):
    $title = $element['NAME'];
    $text = $element['PREVIEW_TEXT'];
    if (!empty($element['PREVIEW_PICTURE'])) {
        $imgSrc = Helper::getResizeImageArray($element['PREVIEW_PICTURE'], 'INDEX_UNIT');
    }
?>
    <section class="capabilities">
        <div class="container">
            <div class="capabilities__inner">
                <div class="capabilities__info">
                    <h2 class="h2 capabilities__title"><?= $title; ?></h2>
                    <div class="capabilities__desc"><?= $text; ?></div>
                </div>
                <div class="capabilities__list">
                    <? if (!empty($capabilities)): ?>
                        <? foreach ($capabilities as $capability): ?>
                            <div class="capabilities__item"><i class="icon icon-round-check capabilities__item-icon"></i>
                                <span class="capabilities__item-title"><?= $capability['UF_NAME']; ?></span>
                            </div>
                        <? endforeach; ?>
                    <? endif; ?>
                </div>
                <div class="capabilities-shcema">
                    <div class="capabilities-brand">
                        <div class="capabilities-brand__connect"></div>
                        <div class="capabilities-brand__block">
                            <div class="capabilities-brand__inner">
                                <div class="capabilities-brand__text">Бренды, продавцы</div>
                            </div>
                        </div>
                        <div class="capabilities-brand__icons"><img src="<?= DIST_SITE_DIR_PATH; ?>img/capabilities/brands.svg" alt="Бренды, продавцы"></div>
                    </div>
                    <div class="capabilities-cloud">
                        <div class="capabilities-cloud__connect-left"></div>
                        <div class="capabilities-cloud__icons"><img src="<?= DIST_SITE_DIR_PATH; ?>img/capabilities/cloud.svg" alt="Ecompass"></div>
                        <div class="capabilities-cloud__connect-right"></div>
                    </div>
                    <div class="capabilities-group">
                        <div class="capabilities-market">
                            <div class="capabilities-market__connect-left"></div>
                            <div class="capabilities-market__icons">
                                <img src="<?= DIST_SITE_DIR_PATH; ?>img/capabilities/markets.svg" alt="Маркетплейсы">
                            </div>
                            <div class="capabilities-market__block">
                                <div class="capabilities-market__inner">
                                    <div class="capabilities-market__text">Маркетплейсы</div>
                                </div>
                            </div>
                            <div class="capabilities-market__connect-bottom"></div>
                        </div>
                        <div class="capabilities-buyer">
                            <div class="capabilities-buyer__connect-top"></div>
                            <div class="capabilities-buyer__img"><img src="<?= DIST_SITE_DIR_PATH; ?>img/capabilities/buyer.png" alt="Покупатель"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif; ?>
