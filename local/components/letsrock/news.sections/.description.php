<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => 'Список разделов',
    "DESCRIPTION" => 'Список разделов верхнего уровня',
    "PATH" => array(
        "ID" => "bitrix",
        "CHILD" => array(
            "ID" => "news.sections",
            "NAME" => "Список разделов верхнего уровня"
        )
    ),
);
?>