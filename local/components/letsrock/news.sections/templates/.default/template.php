<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** Шаблон специально для отображения списка разделов компонента news */
?>

<? if (!empty($arResult['SECTIONS'])): ?>
    <div class="news__menu">
        <? foreach ($arResult['SECTIONS'] as $section):
            $isCurrentSection = $section['ACTIVE_SECTION_CODE'] == 'Y';
            ?>
            <a class="news__menu-item<?= $isCurrentSection ? ' active' : ''; ?>"
               href="<?= !$isCurrentSection ? $section['DETAIL_PAGE_URL'] : 'javascript:void(0)'; ?>"
            >
                <? if (!empty($section['NAME'])): ?>
                    <span class="news__menu-name"><?= $section['NAME']; ?></span>
                <? endif; ?>
                <? if (!empty($section['ITEMS_COUNT'])): ?>
                    <span class="news__menu-count"><?= $section['ITEMS_COUNT']; ?></span>
                <? endif; ?>
            </a>
        <? endforeach; ?>
    </div>
<? endif; ?>