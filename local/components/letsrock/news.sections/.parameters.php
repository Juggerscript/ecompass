<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arComponentParameters = [
    "GROUPS" => [
        "MAIN" => [
            "NAME" => "Основные настройки"
        ]
    ],
    "PARAMETERS" => [
        "IBLOCK_ID" => [
            "NAME" => "ID инфоблока",
            "DEFAULT" => IB_NEWS_ID,
        ],
        "ACTIVE_SECTION_CODE" => [
            "NAME" => "Символьный код активного раздела"
        ],
        "DEPTH_LEVEL" => [
            "NAME" => [
                "NAME" => "Глубина отображения разделов",
                "DEFAULT" => 1,
            ]
        ]
    ]
];