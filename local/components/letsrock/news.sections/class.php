<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Loader::includeModule('letsrock.lib');

class NewsSectionsComponent extends CBitrixComponent
{
    public CONST ITEMS_COUNT = 'ITEMS_COUNT';
    public CONST ACTIVE_SECTION_CODE = 'ACTIVE_SECTION_CODE';

    public function onPrepareComponentParams($arParams)
    {
        $arParam['IBLOCK_ID'] = isset($arParams['IBLOCK_ID']) ? (int)$arParams['IBLOCK_ID'] : IB_NEWS_ID;
        $arParams[static::ACTIVE_SECTION_CODE] = isset($arParams['ACTIVE_SECTION_CODE']) ? htmlspecialchars(strip_tags($arParams['ACTIVE_SECTION_CODE'])) : '';
        $arParams['DEPTH_LEVEL'] = isset($arParams['DEPTH_LEVEL']) ? (int)$arParams['DEPTH_LEVEL'] : 1;

        return $arParams;
    }

    public function executeComponent()
    {
        if ($this->startResultCache()) {
            $sectionsRes = CIBlockSection::GetList([], ['IBLOCK_ID' => IB_NEWS_ID, 'DEPTH_LEVEL' => 1]);
            $sectionsRes->SetUrlTemplates('/news/#SECTION_CODE#/');

            $sections = [];
            $sectionsIds = [];
            while ($section = $sectionsRes->GetNext()) {
                $sections[] = $section;
                $sectionsIds[] = $section['ID'];
            }

            if (!empty($sections)) {
                if (!empty($sectionsIds)) {
                    $elements = [];
                    $elementsIds = [];
                    $elementsRes = CIBlockElement::GetList([], ['IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'IBLOCK_SECTION_ID' => $sectionsIds]);
                    while ($element = $elementsRes->GetNext()) {
                        $elements[] = $element;
                        $elementsIds[] = $element['ID'];
                    }

                    // получаем список разделов, в которых лежит элемент
                    $elementsGroupsIdsRes = CIBlockElement::GetElementGroups($elementsIds, false, ['ID', 'IBLOCK_ELEMENT_ID']);
                    $elementsGroups = [];
                    while ($elementGroups = $elementsGroupsIdsRes->GetNext()) {
                        $elementsGroups[] = $elementGroups;
                    }
                    $sectionsAndElementsIdsArray = $this->connectSectionsAndElementsIds($elementsGroups);
                    foreach($sections as &$section) {
                        $section[static::ITEMS_COUNT] = 0;
                        if ($section['CODE'] == $this->arParams[static::ACTIVE_SECTION_CODE]) $section['ACTIVE_SECTION_CODE'] = 'Y';
                        $currentSectionWithElementsIds = $sectionsAndElementsIdsArray[$section['ID']];
                        foreach ($elements as $element) {
                            if (in_array($element['ID'], $currentSectionWithElementsIds)) $section[static::ITEMS_COUNT] ++;
                        }
                    }
                    $this->arResult['SECTIONS'] = $sections;
                }
            }
            $this->IncludeComponentTemplate();
        }
    }

    /** Метод для работы с массивом, полученным в результате работы метода CIBlockElement::GetElementGroups
     * используется для того, чтобы узнать, в каких разделах лежат элементы.
     * Возвращает массив вида [['SECTION_ID' => ['element1Id', 'element2Id']]]
     */
    private function connectSectionsAndElementsIds($elementsGroups)
    {
        if (empty($elementsGroups)) return false;
        $res = [];
        foreach($elementsGroups as $elementsGroup) {
            $sectionId = $elementsGroup['ID'];
            if (
                empty($res[$sectionId])
                ||
                    (
                        key_exists($sectionId, $res)
                        && !empty($elementsGroup['IBLOCK_ELEMENT_ID'])
                    )
                ) {
                $res[$sectionId][] = $elementsGroup['IBLOCK_ELEMENT_ID'];
            }
        }

        return $res;
    }
}