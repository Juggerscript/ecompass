<?
use Letsrock\Lib\Models\PropertyHL;
/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();

//Собираем свойства из HL

$difficultiesProps = $arResult['PROPERTIES'][PROPERTY_SERVICE_DIFFICULTIES]['VALUE'];
if (!empty($difficultiesProps)) {
    $difficulties = PropertyHL::getHlProperties(HL_SERVICES_DIFFICULTIES_ID, ['UF_XML_ID' => $difficultiesProps]);
    $arResult[PROPERTY_SERVICE_DIFFICULTIES] = $difficulties;
}
?>

