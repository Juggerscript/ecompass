<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Loader;
use \Letsrock\Lib\Models\Helper;
Loader::includeModule('letsrock.lib');
?>
<? if (!empty($arResult)):
    $properties = $arResult['PROPERTIES'];
    if (!empty($arResult['PREVIEW_PICTURE'])) {
        $imgSrc = Helper::getResizeImageArray(
            $arResult['PREVIEW_PICTURE'],
            "SERVICES_LOGISTICS_WINDOW_DETAIL"
        );
    }
    $GLOBALS['personsInChargeIds'] = $properties[PROPERTY_SERVICE_PEOPLES_IN_CHARGE]['VALUE'];
    $detailText = $arResult['DETAIL_TEXT'];
    $title = $arResult['NAME'];
    $titleDesc = $arResult['PREVIEW_TEXT'];
    $subtitle = $properties[PROPERTY_SERVICE_SUBTITLE]['VALUE'];
    $connectionText = $properties[PROPERTY_SERVICE_CONNECTION_TEXT]['VALUE'];
    $thesis = $properties[PROPERTY_SERVICE_THESIS]['VALUE']['TEXT'];
    $difficulties = $arResult[PROPERTY_SERVICE_DIFFICULTIES];
    ?>
    <section class="logistic section-title">
        <div class="container">
            <div class="logistic__content">
                <? if (!empty($title)): ?>
                    <h1 class="h1 logistic__title"><?= $title; ?></h1>
                <? endif; ?>
                <? if (!empty($subtitle)): ?>
                    <div class="logistic__slogan"><?= $subtitle; ?></div>
                <? endif; ?>
                <? if (!empty($titleDesc)): ?>
                    <div class="logistic__desc">
                        <?= $titleDesc; ?>
                    </div>
                <? endif; ?>
                <div class="logistic__button">
                    <a class="button-feedback" href="javascript:void(0)" data-modal-open="feedback">
                        <i class="icon icon-round-right button-feedback__icon"></i>
                        <span class="button-feedback__text"><?= $connectionText; ?></span>
                    </a>
                </div>
            </div>
            <? if (!empty($imgSrc)): ?>
                <div class="logistic__window">
                    <img src="<?= $imgSrc; ?>" alt="">
                </div>
            <? endif; ?>
        </div>
    </section>


    <section class="logistic-info">
        <div class="container">
            <div class="logistic-info__content">
                <? if (!empty($thesis)): ?>
                    <h2 class="h2 logistic-info__title"><?= $thesis; ?></h2>
                <? endif; ?>
                <div class="logistic-info__desc">
                    <?= $detailText; ?>
                </div>
            </div>
        </div>
    </section>

    <? if (!empty($difficulties)): ?>
        <section class="section section--gray logistic-option">
            <div class="container">
                <div class="section__inner">
                    <ul class="option-list">
                        <? foreach ($difficulties as $difficulty): ?>
                            <li class="option-list__item logistic__formatted">
                                <div class="option-list__item-content">
                                    <h3><?= $difficulty['UF_NAME']; ?></h3>
                                    <?= $difficulty['UF_DESCRIPTION']; ?>
                                    <blockquote><?= $difficulty['UF_FULL_DESCRIPTION']; ?></blockquote>
                                </div>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </div>
            </div>
        </section>
    <? endif; ?>

<? endif; ?>
