<?
use Letsrock\Lib\Models\PropertyHL;
/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();

//Собираем свойства из HL
$resProps = $arResult['PROPERTIES'];
$marketplacePlacementProps = $resProps[PROPERTY_SERVICE_MARKETPLACE_PLACEMENT]['VALUE'];
if (!empty($marketplacePlacementProps)) {
    $marketplacePlacement = PropertyHL::getHlProperties(HL_SERVICE_MARKETPLACE_PLACEMENT_ID, ['UF_XML_ID' => $marketplacePlacementProps]);
}

$ownPlacementProps = $resProps[PROPERTY_SERVICE_OWN_PLACEMENT]['VALUE'];
if (!empty($ownPlacementProps)) {
    $ownPlacement = PropertyHL::getHlProperties(HL_SERVICE_OWN_PLACEMENT_ID, ['UF_XML_ID' => $ownPlacementProps]);
}

$fbsPlacementProps = $resProps[PROPERTY_SERVICE_FBS_CENTER_PLACEMENT]['VALUE'];
if (!empty($fbsPlacementProps)) {
    $fbsPlacement = PropertyHL::getHlProperties(HL_SERVICE_FBS_CENTER_PLACEMENT_ID, ['UF_XML_ID' => $fbsPlacementProps]);
}

$arResult['PLACEMENT_TABLE'] = [
    PROPERTY_SERVICE_MARKETPLACE_PLACEMENT => [
        'title' => $resProps[PROPERTY_SERVICE_MARKETPLACE_PLACEMENT]['NAME'],
        'code' => $resProps[PROPERTY_SERVICE_MARKETPLACE_PLACEMENT]['CODE'],
        'elements' => $marketplacePlacement
    ],
    PROPERTY_SERVICE_OWN_PLACEMENT => [
        'title' => $resProps[PROPERTY_SERVICE_OWN_PLACEMENT]['NAME'],
        'code' => $resProps[PROPERTY_SERVICE_OWN_PLACEMENT]['CODE'],
        'elements' => $ownPlacement
    ],
    PROPERTY_SERVICE_FBS_CENTER_PLACEMENT => [
        'title' => $resProps[PROPERTY_SERVICE_FBS_CENTER_PLACEMENT]['NAME'],
        'code' => $resProps[PROPERTY_SERVICE_FBS_CENTER_PLACEMENT]['CODE'],
        'elements' => $fbsPlacement
    ],
]

?>

