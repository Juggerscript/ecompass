<?
use Letsrock\Lib\Models\PropertyHL;
use Letsrock\Lib\Models\Element;
/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();

$resProps = $arResult['PROPERTIES'];
$analyticsIds = $resProps[PROPERTY_SERVICE_ANALYTICS]['VALUE'];

$analyticsTitlePropertyTitle = 'PROPERTY_' . PROPERTY_SERVICE_ANALYTICS_TITLE;
$analyticsIncludePropertyTitle = 'PROPERTY_' . PROPERTY_SERVICE_ANALYTICS_INCLUDE;
$analyticsIncludePropertyTitleValue = $analyticsIncludePropertyTitle . '_VALUE';

$analyticsElements = Element::getList(
    IB_ANALYTICS_ID,
    [
        'ID',
        'NAME',
        $analyticsTitlePropertyTitle,
        $analyticsIncludePropertyTitle,
    ],
    [
        'ID' => $analyticsIds
    ]
);
if (!empty($analyticsElements)) {
    $analyticsIncludeCodes = [];

    foreach ($analyticsElements as $includeElement) {
        $includeCodes = $includeElement[$analyticsIncludePropertyTitleValue];
        if (!empty($includeCodes)) {
            $analyticsIncludeCodes = array_merge($analyticsIncludeCodes, $includeCodes);
        }
    }

    // достаем элементы из хайлода

    $analyticsIncludeHLElements = PropertyHL::getHlProperties(HL_SERVICE_ANALYTICS_ID, ['UF_XML_ID' => $analyticsIncludeCodes]);
    $analyticsRatesHLElements = PropertyHL::getHlProperties(HL_SERVICE_ANALYTICS_RATES_ID);
    $arResult['ANALYTICS_RATES'] = $analyticsRatesHLElements;

    foreach ($analyticsIncludeHLElements as $analyticsIncludeHLElement) {
        foreach($analyticsElements as &$analyticsElement) {
            if(in_array($analyticsIncludeHLElement['UF_XML_ID'], $analyticsElement[$analyticsIncludePropertyTitleValue])) {
                $analyticsElement[PROPERTY_SERVICE_ANALYTICS_INCLUDE][] = $analyticsIncludeHLElement;
            }
        }
    }

    $arResult[PROPERTY_SERVICE_ANALYTICS] = $analyticsElements;
}

$keyPoints = $resProps[PROPERTY_SERVICE_KEY_POINTS]['VALUE'];

if (!empty($keyPoints)) {
    $keyPointsInHL = PropertyHL::getHlProperties(HL_SERVICE_KEY_POINTS_ID, ['UF_XML_ID' => $keyPoints]);
    if (!empty($keyPointsInHL)) $arResult[PROPERTY_SERVICE_KEY_POINTS] = $keyPointsInHL;
}
?>