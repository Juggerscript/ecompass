<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<? if (!empty($arResult)):
    $name = $arResult['NAME'];
    $resProps = $arResult['PROPERTIES'];
    $GLOBALS['personsInChargeIds'] = $resProps[PROPERTY_SERVICE_PEOPLES_IN_CHARGE]['VALUE'];
    $subtitle = $resProps[PROPERTY_SERVICE_SUBTITLE]['VALUE'];
    $connectionText = $resProps[PROPERTY_SERVICE_CONNECTION_TEXT]['VALUE'];
    $titleDesc = $arResult['PREVIEW_TEXT'];
    $previewSlider = $resProps[PROPERTY_SERVICE_PREVIEW_SLIDER]['VALUE'];
    $howItWorksSlider = $resProps[PROPERTY_SERVICE_HOW_IT_WORKS_SLIDER]['VALUE'];
    $thesis = $resProps[PROPERTY_SERVICE_THESIS]['VALUE']['TEXT'];
    $keyPoints = $arResult[PROPERTY_SERVICE_KEY_POINTS];
    $keyPointsTitle = $resProps[PROPERTY_SERVICE_KEY_POINTS]['NAME'];
    $analytics = $arResult[PROPERTY_SERVICE_ANALYTICS];
    ?>
    <section class="logistic section-title logistic--with-slider">
        <div class="container">
            <div class="logistic__content">
                <h1 class="h1 logistic__title"><?= $arResult['NAME']; ?></h1>
                <? if (!empty($subtitle)): ?>
                    <div class="logistic__slogan"><?= $subtitle; ?></div>
                <? endif; ?>
                <? if (!empty($previewSlider)): ?>
                    <div class="logistic__slider swiper-container" data-controller="slider-logistic">
                        <i class="icon icon-arrow-left logistic__slider-prev" data-slider-logistic-target="prev"></i>
                        <div class="logistic__slider-list swiper-wrapper">
                            <? foreach ($previewSlider as $previewSliderText): ?>
                                <div class="logistic__slider-item swiper-slide">
                                    <div class="logistic__slider-item-content"><?= $previewSliderText; ?></div>
                                </div>
                            <? endforeach; ?>
                        </div><i class="icon icon-arrow-right logistic__slider-next" data-slider-logistic-target="next"></i>
                        <div class="logistic__slider-nav">
                            <div class="logistic__slider-pagination swiper-pagination"></div>
                        </div>
                    </div>
                <? endif; ?>
                <? if (!empty($titleDesc)): ?>
                    <div class="logistic__desc">
                        <p><?= $titleDesc; ?></p>
                    </div>
                <? endif; ?>
                <div class="logistic__button">
                    <a class="button-feedback" href="javascript:void(0)" data-modal-open="feedback">
                        <i class="icon icon-round-right button-feedback__icon"></i>
                        <span class="button-feedback__text"><?= $connectionText; ?></span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <? if (!empty($thesis)): ?>
        <section class="logistic-info">
            <div class="container">
                <div class="logistic-info__content">
                    <h2 class="h2 logistic-info__title"><?= $thesis; ?></h2>
                </div>
            </div>
        </section>
    <? endif; ?>
    <? if (!empty($howItWorksSlider)): ?>
        <section class="section section--gray logistic--with-slider">
            <div class="container">
                <div class="section__inner">
                    <ul class="option-list">
                        <? foreach ($howItWorksSlider as $howItWorksText): ?>
                            <li class="option-list__item option-list__item--mini logistic__formatted">
                                <div class="option-list__item-content">
                                    <p><?= $howItWorksText; ?></p>
                                </div>
                            </li>
                        <? endforeach; ?>
                    </ul>
                    <div class="logistic__total logistic--with-slider">
                        <div class="logistic__content">
                            <div class="logistic__total-inner logistic__formatted">
                                <h3>Как итог:</h3>
                                <div class="logistic__total-message">Вы принимаете решения на основе полных данных и опыта специалистов, а не интуитивно.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <? endif; ?>
    <? if (!empty($keyPoints)): ?>
        <section class="cards-section section">
            <div class="container">
                <div class="cards-section__head section-common__head">
                    <? if (!empty($keyPointsTitle)): ?>
                        <div class="cards-section__title section-common__title"><?= $keyPointsTitle; ?></div>
                    <? endif; ?>
                    <div class="cards-section__desc section-common__desc">
                        Вам не придется искать ответы на ключевые вопросы в разных местах и анализировать разнородные данные для управленческих решений.
                        Даже при размещении на нескольких площадках!
                    </div>
                </div>

                <div class="cards-section__list">
                    <? foreach ($keyPoints as $keyPoint): ?>
                        <div class="cards-section__item">
                            <div class="cards-section__item-title"><?= $keyPoint['UF_NAME']; ?></div>
                            <div class="cards-section__item-text"><?= $keyPoint['UF_DESCRIPTION']; ?></div>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </section>

    <? endif; ?>
    <? if (!empty($analytics)):
        $analyticsRates = $arResult['ANALYTICS_RATES'];

        $standardRate = array_shift(array_filter($analyticsRates, function($item) {
            return $item['UF_XML_ID'] == 'rate_standard';
        }));
        $fullRate = array_shift(array_filter($analyticsRates, function($item) {
            return $item['UF_XML_ID'] == 'rate_full';
        }));
        ?>
        <section class="table-section section">
            <div class="container">
                <div class="table-section__head section-common__head">
                    <div class="table-section__title section-common__title">Самая полная кроссплатформенная аналитика</div>
                    <div class="table-section__desc section-common__desc">Тарифы и состав услуг аналитики</div>
                </div>
                <div class="table-section__table">
                    <section class="table-wrap">
                        <div class="table-title">Аналитика категорий маркетплейсов (цена за 1 месяц, за 1 категорию, с НДС)</div>
                        <div class="table">
                            <div class="table__head">
                                <div class="table__row">
                                    <div class="table__col name">Название услуги</div>
                                    <div class="table__col">Что включено</div>
                                    <? if (!empty($standardRate)): ?>
                                        <div class="table__col value">
                                            <?= $standardRate['UF_NAME']; ?>
                                            <div class="table__head-sub"><?= $standardRate['UF_VALUE']; ?> ₽</div>
                                        </div>
                                    <? endif; ?>
                                    <? if (!empty($fullRate)): ?>
                                        <div class="table__col value">
                                            <?= $fullRate['UF_NAME']; ?>
                                            <div class="table__head-sub"><?= $fullRate['UF_VALUE']; ?> ₽</div>
                                        </div>
                                    <? endif; ?>

                                </div>
                            </div>

                            <div class="table__body">
                                <?
                                    $plusPostfix = '__plus';
                                    $minusPostfix = '__minus';
                                    $ufStandard = 'UF_STANDARD';
                                    $ufFull = 'UF_FULL';
                                ?>
                                <? foreach ($analytics as $analytic):
                                    $analyticIncludeCountMoreThanOne = count($analytic[PROPERTY_SERVICE_ANALYTICS_INCLUDE]) > 1;
                                    ?>
                                    <div class="table__row">
                                        <div class="table__col name" data-title="Название услуги">
                                            <b><?= $analytic['NAME']; ?></b>
                                        </div>
                                        <div class="table__col" data-title="Что включено">
                                            <? if ($analyticIncludeCountMoreThanOne): ?>
                                                <? foreach ($analytic[PROPERTY_SERVICE_ANALYTICS_INCLUDE] as $includeItem): ?>
                                                    <div class="table__col-item"><?= $includeItem['UF_NAME']; ?></div>
                                                <? endforeach; ?>
                                            <? else: ?>
                                                <?= $analytic[PROPERTY_SERVICE_ANALYTICS_INCLUDE][0]['UF_NAME']; ?>
                                            <? endif; ?>
                                        </div>
                                        <div class="table__col value" data-title="<?= $standardRate['UF_NAME']; ?>">
                                            <? if ($analyticIncludeCountMoreThanOne): ?>
                                                <? foreach ($analytic[PROPERTY_SERVICE_ANALYTICS_INCLUDE] as $includeItem): ?>
                                                    <div class="table__col-item">
                                                        <span class="table<?= $includeItem[$ufStandard] ? $plusPostfix : $minusPostfix; ?>">
                                                            <?= $includeItem[$ufStandard] ? '+' : '–'; ?>
                                                        </span>
                                                    </div>
                                                <? endforeach; ?>
                                            <? else: ?>
                                                <span class="table<?= $analytic[PROPERTY_SERVICE_ANALYTICS_INCLUDE][0][$ufStandard] ? $plusPostfix : $minusPostfix; ?>">
                                                    <?= $analytic[PROPERTY_SERVICE_ANALYTICS_INCLUDE][0][$ufStandard] ? '+' : '–'; ?>
                                                </span>
                                            <? endif; ?>
                                        </div>
                                        <div class="table__col value" data-title="<?= $fullRate['UF_NAME']; ?>">
                                            <? if ($analyticIncludeCountMoreThanOne): ?>
                                                <? foreach ($analytic[PROPERTY_SERVICE_ANALYTICS_INCLUDE] as $includeItem): ?>
                                                    <div class="table__col-item">
                                                        <span class="table<?= $includeItem[$ufFull] ? $plusPostfix : $minusPostfix; ?>">
                                                            <?= $includeItem[$ufFull] ? '+' : '–'; ?>
                                                        </span>
                                                    </div>
                                                <? endforeach; ?>
                                            <? else: ?>
                                                <span class="table<?= $analytic[PROPERTY_SERVICE_ANALYTICS_INCLUDE][0][$ufFull] ? $plusPostfix : $minusPostfix; ?>">
                                                    <?= $analytic[PROPERTY_SERVICE_ANALYTICS_INCLUDE][0][$ufFull] ? '+' : '–'; ?>
                                                </span>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>

    <? endif; ?>
<? endif; ?>
