<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Letsrock\Lib\Models\Helper;
$items = $arResult['ITEMS'];
?>

<? if (!empty($items)): ?>
    <section class="section">
        <div class="container">
            <h1 class="h1 section__title--center">Ваши процессы на всех маркетплейсах в одном окне</h1>
        </div>
    </section>
    <? foreach ($items as $key => $item):
        $name = $item['NAME'];
        $previewText = $item['PROPERTIES'][PROPERTY_SERVICE_PREVIEW_TEXT_ON_LIST];
        $htmlText = $previewText['VALUE']['TEXT'];
        if (isset($htmlText) && !empty($htmlText)) {
            $text = $htmlText;
        } else {
            $text = $previewText['VALUE'];
        }
        $picture = $item['PROPERTIES'][PROPERTY_SERVICE_PREVIEW_PICTURE_ON_LIST]['VALUE'];
        $imgSrc = Helper::getResizeImageArrayById($picture, 'INDEX_SERVICES_LIST');
    ?>
        <section class="section-half section<?= $key % 2 ? ' section--gray' : ' section-half--desk-first-info'; ?>">

            <div class="container">
                <div class="section-half__inner">
                    <? if (!empty($imgSrc)): ?>
                        <div class="section-half__part">
                            <div class="section-half__img">
                                <img src="<?= $imgSrc ?>" alt="">
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="section-half__part">
                        <? if (!empty($name)): ?>
                            <h2 class="h2 section-half__title"><?= $name; ?></h2>
                        <? endif; ?>
                        <? if (!empty($text)): ?>
                            <div class="section-half__desc">
                                <?= $text; ?>

                            </div>
                        <? endif; ?>
                        <div class="section-half__button">
                            <a class="button button--default" href="<?= $item['DETAIL_PAGE_URL']; ?>">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <? endforeach; ?>
<? endif; ?>
