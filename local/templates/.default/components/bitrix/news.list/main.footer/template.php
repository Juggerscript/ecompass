<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Letsrock\Lib\Models\Helper;
$items = $arResult['ITEMS'];
$curDir = $APPLICATION->GetCurDir();
?>
<? if (!empty($items)): ?>
    <section class="section section--gray marketplace-section">
        <div class="container">
            <h2 class="h2 section__title">
                <?= $curDir == SITE_DIR ? 'Работаем со всеми маркетплейсами как крупнейшими, так и нишевыми категорийными' : 'Работаем со всеми топовыми<br> 
и нишевыми маркетплейсами'; ?>
                </h2>
        </div>
        <div class="container">
            <div class="section__inner">
                <ul class="marketplace-section__list">
                    <? foreach ($items as $item):
                        $itemProps = $item['PROPERTIES'];
                        if (!empty($item['PREVIEW_PICTURE'])) {
                            $imgSrc = Helper::getResizeImageArray($item['PREVIEW_PICTURE'], 'MARKETPLACE_CATALOG_CARDS_IN_FOOTER');
                            $usersPerMonth = $itemProps[PROPERTY_VISITORS_PER_MONTH];
                            $usersPerMonthName = $usersPerMonth['NAME'];
                            $usersPerMonthValue = $usersPerMonth['VALUE'];
                            $ordersPerDay = $itemProps[PROPERTY_ORDERS_PER_DAY];
                            $ordersPerDayName = $ordersPerDay['NAME'];
                            $ordersPerDayValue = $ordersPerDay['VALUE'];
                        }
                        ?>
                        <li class="marketplace-section__item">
                            <a href="<?= $item['DETAIL_PAGE_URL']; ?>" class="marketplace-section__item-link"></a>
                            <div class="marketplace-section__item-logo">
                                <? if ($imgSrc): ?>
                                    <img src="<?= $imgSrc; ?>" alt="">
                                <? endif; ?>
                            </div>
                            <div class="marketplace-section__item-data">
                                <? if (!empty($usersPerMonthName) && !empty($usersPerMonthValue)): ?>
                                    <p>
                                        <b><?= $usersPerMonthValue; ?></b>
                                        <?= $usersPerMonthName; ?>
                                    </p>
                                <? endif; ?>

                                <? if (!empty($ordersPerDayName) && !empty($ordersPerDayValue)): ?>
                                    <p>
                                        <b><?= $ordersPerDayValue; ?></b>
                                        <?= $ordersPerDayName; ?>
                                    </p>
                                <? endif; ?>
                            </div>
                        </li>
                    <? endforeach; ?>
                </ul>

                <? if ($APPLICATION->GetCurDir() != SITE_DIR): ?>
                    <div class="marketplace-section__footer">
                        <div class="marketplace-section__half">
                            <a class="button button--default marketplace-section__footer-button" href="<?= MARKETPLACE_CATALOG_PATH; ?>">Весь каталог маркетплейсов</a>
                        </div>
                        <div class="marketplace-section__half">
                            <div class="marketplace-section__nda">
                                <i class="icon icon-lightning marketplace-section__nda-icon"></i>
                                <span class="marketplace-section__nda-text">NDA включен в контракт</span>
                            </div>
                        </div>
                    </div>
                <? endif; ?>
            </div>
        </div>
    </section>

<? endif; ?>

