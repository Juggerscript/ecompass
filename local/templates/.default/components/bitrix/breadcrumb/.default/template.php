<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$strReturn .= '<section class="breadcrumbs-section" itemprop="http://schema.org/breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList"><div class="container"><ul class="breadcrumbs"><li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a></li>';

$itemSize = count($arResult);

for ($index = 0; $index < $itemSize; $index++) {
    $title = $arResult[$index]['TITLE'];
    $link = $arResult[$index]['LINK'];
    if ($link != "" && $index != $itemSize - 1) {
        $strReturn .= '
		    <li class="breadcrumbs__item" id="bx_breadcrumb_' . $index . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a class="breadcrumbs__link" href="' . $link . '" title="' . $title . '" itemprop="url">'
					. $title .
				'</a>
				<meta itemprop="position" content="' . ($index + 1) . '" />
			</li>';
    } else {
        $strReturn .= '
            <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">'
				. $title .
				'<meta itemprop="position" content="' . ($index + 1) . '" />
			</li>';
    }
}

$strReturn .= '</ul></div></section>';
return $strReturn;
?>
