<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$items = $arResult['ITEMS'];
?>
<? if (!empty($items)): ?>
    <form
            name="<?echo $arResult["FILTER_NAME"]."_form"?>"
            action="<?echo $arResult["SEF_SET_FILTER_URL"]?>" method="get">
        <div class="catalog__filter" data-controller="filter">
            <? foreach ($items as $item):
                $values = $item['VALUES'];?>
                <div class="catalog__toolbar-item" data-filter-target="item">
                    <a class="catalog__toolbar-link"
                       href="javascript:void(0)"
                       data-action="click-&gt;filter#open"
                       data-filter-target="select">
                        <span class="catalog__toolbar-name"><?= $item['NAME']; ?></span>
                        <span class="catalog__toolbar-result" data-filter-target="result"></span>
                        <i class="icon icon-arrow-down catalog__toolbar-icon"></i>
                    </a>
                    <div class="catalog__toolbar-dd">
                        <div class="catalog__toolbar-dd-body">
                            <div class="catalog__toolbar-dd-item">
                                <label class="field-checkbox field-checkbox--gray">
                                    <input class="field-checkbox__input" type="checkbox" name="all" data-filter-target="prop">
                                    <div class="field-checkbox__custom"></div>
                                    <div class="field-checkbox__text">Все
                                    </div>
                                </label>
                            </div>
                            <? foreach ($values as $val => $ar): ?>
                                <div class="catalog__toolbar-dd-item">
                                    <label class="field-checkbox field-checkbox--gray">
                                        <input class="field-checkbox__input"
                                               type="checkbox"
                                               data-filter-target="prop"
                                               value="<? echo $ar["HTML_VALUE"] ?>"
                                               name="<? echo $ar["CONTROL_NAME"] ?>"
                                               id="<? echo $ar["CONTROL_ID"] ?>"
                                            <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                        >
                                        <div class="field-checkbox__custom" <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>></div>
                                        <div class="field-checkbox__text"><?= $ar['VALUE']; ?></div>
                                    </label>
                                </div>
                            <? endforeach; ?>
                        </div>
                        <div class="catalog__toolbar-dd-footer">
                            <input
                                    class="button button--mini catalog__toolbar-dd-button"
                                    type="submit"
                                    id="set_filter"
                                    name="set_filter"
                                    value="Показать"
                            />
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </form>
<? endif; ?>