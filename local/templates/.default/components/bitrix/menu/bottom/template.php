<? if (!empty($arResult)): ?>
    <div class="footer__dd footer__dd--menu" data-controller="dd" data-dd-is-mobile-only="data-dd-is-mobile-only">
        <div class="footer__dd-title" data-action="click-&gt;dd#toggle">Разделы</div>
        <ul class="footer__dd-list" data-dd-target="list">
            <? foreach($arResult as $resItem): ?>
                <li class="footer__dd-item"><a class="footer__dd-link" href="<?= $resItem['LINK']; ?>"><?= $resItem['TEXT'] ?></a></li>
            <? endforeach; ?>
            </ul>
    </div>
<? endif; ?>