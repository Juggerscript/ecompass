<? if (!empty($arResult)): ?>
    <ul class="header__menu">
        <? foreach($arResult as $resItem): ?>
            <li class="header__menu-item">
                <a href="<?= $resItem['LINK'] ?>" class="header__menu-link"><?= $resItem['TEXT'] ?></a>
            </li>
        <? endforeach; ?>
    </ul>
<? endif; ?>
