<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Loader;
use \Letsrock\Lib\Models\Helper;
Loader::includeModule('letsrock.lib');
$items = $arResult['ITEMS'];
?>
    <div class="catalog__list">
        <? if (!empty($items)): ?>
            <? foreach ($items as $item):
                if (!empty($item['PREVIEW_PICTURE'])) {
                    $imgSrc = Helper::getResizeImageArray($item['PREVIEW_PICTURE'], 'MARKETPLACE_CATALOG_CARDS_IN_LIST');
                }
                $itemProps = $item['PROPERTIES'];
                //Display ONLY the first specialization
                $itemSpecialization = $item[PROPERTY_SPECIALIZATION][0];
                $itemSpecializationName = $itemSpecialization['UF_NAME'];
                $itemSpecializationImgSrc = Helper::getResizeImageArrayById(
                        $itemSpecialization['UF_FILE'],
                        'MARKETPLACE_CATALOG_SPECIALIZATIONS_IN_LIST'
                );
                $itemIsPartner = $itemProps[PROPERTY_IS_PARTNER]['VALUE'];

                $statPropsToShow = [
                    $itemProps[PROPERTY_VISITORS_PER_MONTH],
                    $itemProps[PROPERTY_BUYERS],
                    $itemProps[PROPERTY_ORDERS_PER_DAY],
                ]; ?>
                <article class="catalog__item">
                    <div class="catalog__item-header">
                        <? if (!empty($imgSrc)): ?>
                            <div class="catalog__item-img-wrap">
                                <img class="catalog__item-img" src="<?= $imgSrc; ?>" alt="">
                            </div>
                        <? endif; ?>

                        <ul class="catalog__item-option-list">
                            <? if (!empty($statPropsToShow)): ?>
                                <? foreach ($statPropsToShow as $itemPropToShow): ?>
                                    <? if (!empty($itemPropToShow['NAME']) && !empty($itemPropToShow['VALUE'])): ?>
                                            <li class="catalog__item-option">
                                                <? if (!empty($itemPropToShow['NAME'])): ?>
                                                    <span class="catalog__item-option-key"><?= $itemPropToShow['NAME']; ?></span>
                                                <? endif; ?>
                                                <? if (!empty($itemPropToShow['VALUE'])): ?>
                                                    <span class="catalog__item-option-value"><?= $itemPropToShow['VALUE']; ?></span>
                                                <? endif; ?>
                                            </li>
                                    <? endif; ?>
                                <? endforeach; ?>

                            <? endif; ?>
                        </ul>
                    </div>
                    <div class="catalog__item-footer">
                        <div class="catalog__item-props">
                            <? if ($itemSpecializationName): ?>
                                <div class="catalog__item-type">
                                    <? if (!empty($itemSpecializationImgSrc)): ?>
                                        <img src="<?= $itemSpecializationImgSrc; ?>" alt="">
                                    <? endif; ?>
<!--                                            <i class="icon icon-supermarket catalog__item-type-icon"></i>-->
                                    <span class="catalog__item-type-name"><?= $itemSpecializationName; ?></span>
                                </div>
                            <? endif; ?>

                            <? if ($itemIsPartner): ?>
                                <div class="catalog__item-partner">
                                    <i class="icon icon-partner catalog__item-partner-icon"></i>
                                    <span class="catalog__item-partner-text"><?= $itemProps[PROPERTY_IS_PARTNER]['NAME']; ?></span>
                                </div>
                            <? endif; ?>
                        </div>
                        <div class="catalog__item-button">
                            <a class="button button--mini" href="<?= $item['DETAIL_PAGE_URL']; ?>">Подробнее</a>
                        </div>
                    </div>
                </article>
            <? endforeach; ?>
        <? endif; ?>
    </div>

    <? if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <?= $arResult["NAV_STRING"]; ?>
    <? endif; ?>
