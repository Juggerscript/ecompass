<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Letsrock\Lib\Models\PropertyHL;

$items = &$arResult['ITEMS'];
$specializationsCodes = [];
if (!empty($items)) {
    foreach($arResult['ITEMS'] as $item) {
        if (!empty($item['PROPERTIES'][PROPERTY_SPECIALIZATION]['VALUE']))
            $specializationsCodes = array_merge($specializationsCodes, $item['PROPERTIES'][PROPERTY_SPECIALIZATION]['VALUE']);
    }


    if (!empty($specializationsCodes)) {
        $specializationsCodes = array_unique($specializationsCodes);
        $specializationsInHl = PropertyHL::getHlProperty(HL_MARKETPLACE_SPECIALIZATION_ID, ['UF_XML_ID' => $specializationsCodes]);
        
        if (!empty($specializationsInHl)) {
            foreach ($specializationsInHl as $specializationInHl) {
                foreach ($items as &$item) {
                    if (in_array($specializationInHl['UF_XML_ID'], $item['PROPERTIES'][PROPERTY_SPECIALIZATION]['VALUE'])) {
                        $item[PROPERTY_SPECIALIZATION][] = $specializationInHl;
                    }
                }
            }
        }
    }
}
