<?
use Letsrock\Lib\Models\PropertyHL;
/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
//Собираем преимущества
$advantagesCodes = $arResult['PROPERTIES'][PROPERTY_MARKETPLACE_ADVANTAGES]['VALUE'];
if (!empty($advantagesCodes)) {
    $advantages = PropertyHL::getHlProperties(
        HL_MARKETPLACE_ADVANTAGES_ID,
        ['UF_XML_ID' => $advantagesCodes]
    );
    $arResult[PROPERTY_MARKETPLACE_ADVANTAGES] = $advantages;
}
?>

