<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<form
    name="<?echo $arResult["FILTER_NAME"]."_form"?>"
    action="<?echo $arResult["FORM_ACTION"]?>" method="get">
    <div class="catalog__filter" data-controller="filter">
        <div class="catalog__toolbar-item" data-filter-target="item">
            <a class="catalog__toolbar-link"
               href="javascript:void(0)"
               data-action="click-&gt;filter#open"
               data-filter-target="select">
                <span class="catalog__toolbar-name">Нишевые</span>
                <span class="catalog__toolbar-result" data-filter-target="result"></span>
                <i class="icon icon-arrow-down catalog__toolbar-icon"></i>
            </a>
            <div class="catalog__toolbar-dd">
                <div class="catalog__toolbar-dd-body">
                    <div class="catalog__toolbar-dd-item">
                        <label class="field-checkbox field-checkbox--gray">
                            <input class="field-checkbox__input" type="checkbox" name="all" data-filter-target="prop">
                            <div class="field-checkbox__custom"></div>
                            <div class="field-checkbox__text">Все
                            </div>
                        </label>
                    </div>
                    <div class="catalog__toolbar-dd-item">
                        <label class="field-checkbox field-checkbox--gray">
                            <input class="field-checkbox__input" type="checkbox" name="clothes" data-filter-target="prop">
                            <div class="field-checkbox__custom"></div>
                            <div class="field-checkbox__text">Одежда
                            </div>
                        </label>
                    </div>
                    <div class="catalog__toolbar-dd-item">
                        <label class="field-checkbox field-checkbox--gray">
                            <input class="field-checkbox__input" type="checkbox" name="shoes" data-filter-target="prop">
                            <div class="field-checkbox__custom"></div>
                            <div class="field-checkbox__text">Обувь
                            </div>
                        </label>
                    </div>
                    <div class="catalog__toolbar-dd-item">
                        <label class="field-checkbox field-checkbox--gray">
                            <input class="field-checkbox__input" type="checkbox" name="sports-goods" data-filter-target="prop">
                            <div class="field-checkbox__custom"></div>
                            <div class="field-checkbox__text">Спорттовары
                            </div>
                        </label>
                    </div>
                    <div class="catalog__toolbar-dd-item">
                        <label class="field-checkbox field-checkbox--gray">
                            <input class="field-checkbox__input" type="checkbox" name="toys" data-filter-target="prop">
                            <div class="field-checkbox__custom"></div>
                            <div class="field-checkbox__text">Игрушки
                            </div>
                        </label>
                    </div>
                </div>
                <div class="catalog__toolbar-dd-footer">
                    <a class="button button--mini catalog__toolbar-dd-button"
                       href="javascript:void(0)" data-action="click-&gt;filter#apply">Показать</a>
                </div>
            </div>
        </div>
        <div class="catalog__toolbar-item" data-filter-target="item">
            <a class="catalog__toolbar-link"
               href="javascript:void(0)"
               data-action="click-&gt;filter#open"
               data-filter-target="select">
                <span class="catalog__toolbar-name">Ключевой ассортимент</span>
                <span class="catalog__toolbar-result" data-filter-target="result"></span>
                <i class="icon icon-arrow-down catalog__toolbar-icon"></i>
            </a>
            <div class="catalog__toolbar-dd">
                <div class="catalog__toolbar-dd-body">
                    <div class="catalog__toolbar-dd-item">
                        <label class="field-checkbox field-checkbox--gray">
                            <input class="field-checkbox__input"
                                   type="checkbox"
                                   name="all"
                                   data-filter-target="prop">
                            <div class="field-checkbox__custom"></div>
                            <div class="field-checkbox__text">Все</div>
                        </label>
                    </div>
                    <div class="catalog__toolbar-dd-item">
                        <label class="field-checkbox field-checkbox--gray">
                            <input class="field-checkbox__input"
                                   type="checkbox"
                                   name="clothes"
                                   data-filter-target="prop">
                            <div class="field-checkbox__custom"></div>
                            <div class="field-checkbox__text">Одежда
                            </div>
                        </label>
                    </div>
                    <div class="catalog__toolbar-dd-item">
                        <label class="field-checkbox field-checkbox--gray">
                            <input class="field-checkbox__input"
                                   type="checkbox"
                                   name="shoes"
                                   data-filter-target="prop">
                            <div class="field-checkbox__custom"></div>
                            <div class="field-checkbox__text">Обувь
                            </div>
                        </label>
                    </div>
                    <div class="catalog__toolbar-dd-item">
                        <label class="field-checkbox field-checkbox--gray">
                            <input class="field-checkbox__input"
                                   type="checkbox"
                                   name="sports-goods"
                                   data-filter-target="prop">
                            <div class="field-checkbox__custom"></div>
                            <div class="field-checkbox__text">Спорттовары</div>
                        </label>
                    </div>
                    <div class="catalog__toolbar-dd-item">
                        <label class="field-checkbox field-checkbox--gray">
                            <input class="field-checkbox__input"
                                   type="checkbox" name="toys"
                                   data-filter-target="prop">
                            <div class="field-checkbox__custom"></div>
                            <div class="field-checkbox__text">Игрушки
                            </div>
                        </label>
                    </div>
                </div>
                <div class="catalog__toolbar-dd-footer">
                    <a class="button button--mini catalog__toolbar-dd-button"
                       href="javascript:void(0)"
                       data-action="click-&gt;filter#apply">Показать</a>
                </div>
            </div>
        </div>
    </div>
</form>



<!--<form name="--><?//echo $arResult["FILTER_NAME"]."_form"?><!--" action="--><?//echo $arResult["FORM_ACTION"]?><!--" method="get">-->
<!--	--><?//foreach($arResult["ITEMS"] as $arItem):
//		if(array_key_exists("HIDDEN", $arItem)):
//			echo $arItem["INPUT"];
//		endif;
//	endforeach;?>
<!--	<table class="data-table" cellspacing="0" cellpadding="2">-->
<!--	<thead>-->
<!--		<tr>-->
<!--			<td colspan="2" align="center">--><?//=GetMessage("IBLOCK_FILTER_TITLE")?><!--</td>-->
<!--		</tr>-->
<!--	</thead>-->
<!--	<tbody>-->
<!--		--><?//foreach($arResult["ITEMS"] as $arItem):?>
<!--			--><?//if(!array_key_exists("HIDDEN", $arItem)):?>
<!--				<tr>-->
<!--					<td valign="top">--><?//=$arItem["NAME"]?><!--:</td>-->
<!--					<td valign="top">--><?//=$arItem["INPUT"]?><!--</td>-->
<!--				</tr>-->
<!--			--><?//endif?>
<!--		--><?//endforeach;?>
<!--	</tbody>-->
<!--	<tfoot>-->
<!--		<tr>-->
<!--			<td colspan="2">-->
<!--				<input type="submit" name="set_filter" value="--><?//=GetMessage("IBLOCK_SET_FILTER")?><!--" /><input type="hidden" name="set_filter" value="Y" />&nbsp;&nbsp;<input type="submit" name="del_filter" value="--><?//=GetMessage("IBLOCK_DEL_FILTER")?><!--" /></td>-->
<!--		</tr>-->
<!--	</tfoot>-->
<!--	</table>-->
<!--</form>-->
