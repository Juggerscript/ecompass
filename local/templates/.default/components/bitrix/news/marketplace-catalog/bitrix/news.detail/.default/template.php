<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Loader;
use \Letsrock\Lib\Models\Helper;
Loader::includeModule('letsrock.lib');
?>
<? if (!empty($arResult)):
    $imgSrc = Helper::getResizeImageArray($arResult['DETAIL_PICTURE'], "MARKETPLACE_CATALOG_CARDS_IN_DETAIL");
    $itemProps = $arResult['PROPERTIES'];
    $text = $arResult['DETAIL_TEXT'];
    $visitorsPerMonth = $itemProps[PROPERTY_VISITORS_PER_MONTH];
    $ordersPerDay = $itemProps[PROPERTY_ORDERS_PER_DAY];
    $productsCount = $itemProps[PROPERTY_PRODUCTS_COUNT];
    $yearsOnMarket = $itemProps[PROPERTY_YEARS_ON_MARKET];
    $advantages = $arResult[PROPERTY_MARKETPLACE_ADVANTAGES];

    $previewProperties = [
        $visitorsPerMonth,
        $ordersPerDay,
        $productsCount,
        $yearsOnMarket
    ]; ?>
    <section class="catalog-detail">
        <div class="container">
            <div class="catalog-detail__head">
                <div class="catalog-detail__logo">
                    <? if (!empty($imgSrc)): ?>
                        <div class="catalog-detail__logo-img"><img src="<?= $imgSrc; ?>" alt=""></div>
                    <? endif; ?>
                </div>
                <div class="catalog-detail__desc">
                    <? if (!empty($text)): ?>
                        <div class="catalog-detail__text"><?= $text; ?></div>
                    <? endif; ?>
                    <div class="catalog-detail__prop-list">
                        <? foreach ($previewProperties as $previewProperty): ?>
                            <? if (!empty($previewProperty['NAME']) && !empty($previewProperty['VALUE'])): ?>
                                    <div class="catalog-detail__prop">
                                        <div class="catalog-detail__prop-value"><?= $previewProperty['VALUE']; ?></div>
                                        <div class="catalog-detail__prop-name"><?= $previewProperty['NAME']; ?></div>
                                    </div>
                            <? endif; ?>
                        <? endforeach; ?>
                    </div>
                    <div class="catalog-detail__button">
                        <a class="button button--default"
                           href="javascript:void(0)"
                           data-modal-open="feedback">Подключиться</a>
                    </div>
                </div>
            </div>

            <? $marketplaceHistory = $itemProps[PROPERTY_MARKETPLACE_HISTORY];
            $marketplaceCommission = $itemProps[PROPERTY_MARKETPLACE_COMMISSION];
            $marketplaceTechIntegration = $itemProps[PROPERTY_MARKETPLACE_TECH_INTEGRATION];

            $marketPlaceBodyWrapItems = [$marketplaceHistory, $marketplaceTechIntegration, $marketplaceCommission];
            ?>
            <div class="catalog-detail__body-wrap">
                <div class="catalog-detail__body">
                    <? if (!empty($marketPlaceBodyWrapItems)): ?>
                        <div class="catalog-detail__tab-block">
                                <div class="catalog-detail__tab-list">
                                    <? foreach ($marketPlaceBodyWrapItems as $bodyWrapItem):
                                        $bodyWrapItemName = $bodyWrapItem['NAME'];
                                        $bodyWrapItemValue = $bodyWrapItem['~VALUE']['TEXT'];
                                        $bodyWrapItemCode = $bodyWrapItem['CODE'];
                                        if (empty($bodyWrapItemName) || empty($bodyWrapItemCode)) continue;
                                    ?>
                                        <? if (!empty($bodyWrapItemName)): ?>
                                            <a class="catalog-detail__tab" href="#<?= $bodyWrapItemCode; ?>">
                                                <span class="catalog-detail__tab-name"><?= $bodyWrapItemName; ?></span>
                                            </a>
                                        <? endif; ?>
                                    <? endforeach; ?>
                                </div>
                        </div>
                    <? endif; ?>

                    <? if (!empty($marketPlaceBodyWrapItems)): ?>
                        <div class="catalog-detail__content text">
                            <? foreach ($marketPlaceBodyWrapItems as $bodyWrapItem):
                                $bodyWrapItemName = $bodyWrapItem['NAME'];
                                $bodyWrapItemValue = $bodyWrapItem['~VALUE']['TEXT'];
                                $bodyWrapItemCode = $bodyWrapItem['CODE'];
                                if (empty($bodyWrapItemName) || empty($bodyWrapItemValue)) continue;
                            ?>
                                <div class="text-nav" id="<?= $bodyWrapItemCode; ?>">
                                    <h2><?= $bodyWrapItemName; ?></h2>
                                    <?= $bodyWrapItemValue; ?>

                                    <? if ($bodyWrapItem['CODE'] == PROPERTY_MARKETPLACE_COMMISSION &&  !empty($advantages)): ?>
                                        <div class="table-simple">
                                            <div class="table-simple__row table-simple__head">
                                                <? foreach ($advantages as $advantage): ?>
                                                    <div class="table-simple__col">
                                                        <b><?= $advantage['UF_NAME']; ?></b>
                                                    </div>
                                                <? endforeach; ?>
                                            </div>
                                            <div class="table-simple__row">
                                                <? foreach ($advantages as $advantage):
                                                    $description = $advantage['UF_DESCRIPTION'];?>
                                                    <div class="table-simple__col" data-head="<?= $description; ?>">
                                                        <p><?= $description; ?></p>
                                                    </div>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                </div>
                            <? endforeach; ?>
                        </div>
                    <? endif; ?>
                    <div class="catalog-detail__footer"><a class="button button--default"
                           href="javascript:void(0)"
                           data-modal-open="feedback">Подключиться</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="catalog-detail__decor">
            <svg width="545" height="1069" viewBox="0 0 545 1069" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <path d="M446 891C446 797.112 369.888 721 276 721H-210V1069H276C369.888 1069 446 992.888 446 899V891Z" fill="#35359A"></path>
                <g filter="url(#filter0_d)">
                    <rect x="-137" y="128.959" width="460.56" height="819.414" transform="rotate(-13.6847 -137 128.959)" fill="url(#pattern0)"></rect>
                </g>
                <defs>
                    <filter id="filter0_d" x="-177" y="0" width="721.342" height="985.111" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                        <feflood flood-opacity="0" result="BackgroundImageFix"></feflood>
                        <fecolormatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"></fecolormatrix>
                        <feoffset dy="20"></feoffset>
                        <fegaussianblur stdDeviation="20"></fegaussianblur>
                        <fecolormatrix type="matrix" values="0 0 0 0 0.207843 0 0 0 0 0.207843 0 0 0 0 0.603922 0 0 0 0.08 0"></fecolormatrix>
                        <feblend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"></feblend>
                        <feblend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"></feblend>
                    </filter>
                    <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                        <use xlink:href="#image0" transform="translate(-0.000328085) scale(0.00100066 0.00056243)"></use>
                    </pattern>
                    <image id="image0" width="1000" height="1778" xlink:href="<?= DIST_SITE_DIR_PATH; ?>img/catalog-detail/site.png"></image>
                </defs>
            </svg>
        </div>
    </section>
<? endif; ?>
