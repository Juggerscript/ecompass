<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Loader;
use \Letsrock\Lib\Models\Helper;
use \Letsrock\Lib\Models\Http;
Loader::includeModule('letsrock.lib');
?>
<? if (!empty($arResult)):
    $imgSrc = Helper::getResizeImageArray($arResult['DETAIL_PICTURE'], "NEWS_DETAIL_PICTURE");
    $text = $arResult['DETAIL_TEXT'];
    $tags = $arResult['TAGS'];
    $prevItem = $arResult['PREV_ITEM'];
    $nextItem = $arResult['NEXT_ITEM'];
    $recommendation = $arResult['RECOMMENDATION'];
    $title = $arResult['NAME'];
    ?>
    <section class="news-detail">
        <div class="container">
            <div class="news-detail__head">
                <? if (!empty($imgSrc)): ?>
                    <div class="news-detail__cover">
                        <img class="news-detail__img" src="<?= $imgSrc ?>" alt="">
                    </div>
                <? endif; ?>

                <div class="news-detail__share">
                    <article class="share share--horizontal-in-mobile" data-controller="share"
                             data-share-url="<?= Http::getFullUrl(); ?>"
                             data-share-title="<?= $title; ?>"
                             data-share-image="<?= Http::getUrl(false) . $imgSrc; ?>"
                             data-share-description="<?= $title; ?>">
                        <div class="share__logo">
                            <i class="icon icon-share share__item"></i>
                        </div>
                        <div class="share__list">
                            <i class="icon icon-vk share__item" data-action="click-&gt;share#start" data-type="vk"></i>
                            <i class="icon icon-fb share__item" data-action="click-&gt;share#start" data-type="fb"></i>
                            <i class="icon icon-ok share__item" data-action="click-&gt;share#start" data-type="ok"></i>
                            <i class="icon icon-whatsapp share__item" data-action="click-&gt;share#start" data-type="whatsapp"></i>
                        </div>
                    </article>
                </div>
            </div>
            <div class="news-detail__body">
                <? if (!empty($text)): ?>
                    <div class="news-detail__content text">
                        <?= $text; ?>
                    </div>
                <? endif; ?>

                <? if(!empty($tags)): ?>
                    <div class="news-detail__tags">
                        <? foreach($tags as $tag): ?>
                            <a class="news-detail__tags-item"
                               href="<?= $arResult['LIST_PAGE_URL'] . 'search/?q=' . $tag['UF_XML_ID'] ; ?>"><?= $tag['UF_NAME'] ?></a>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
                <div class="news-detail__nav">
                    <div class="news-detail__nav-item news-detail__nav-item--left">
                        <a class="news-detail__nav-link" href="<?= !empty($prevItem) ? $prevItem['DETAIL_PAGE_URL'] : 'javascript:void(0)' ?>">
                            <span>Предыдущая новость</span>
                        </a>
                    </div>
                    <div class="news-detail__nav-item news-detail__nav-item--right">
                        <a class="news-detail__nav-link" href="<?= !empty($nextItem) ? $nextItem['DETAIL_PAGE_URL'] : 'javascript:void(0)' ?>">
                            <span>Следующая новость</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <? if(!empty($recommendation)): ?>
        <section class="news-slide">
            <div class="container">
                <h2 class="h2 news__title"><?= $arResult['PROPERTIES']['RECOMMENDATION']['NAME'] ?></h2>
                <div class="news__list">
                    <? foreach($recommendation as $rec):
                    $recImgSrc = Helper::getResizeImageArrayById($rec['PREVIEW_PICTURE'], "NEWS_LIST_PICTURE");
                    $activeFrom = $rec['ACTIVE_FROM'];
                    $timestamp = !empty($activeFrom) ? $activeFrom : $rec['TIMESTAMP_X'];
                    $timeStampStr = Helper::timestampXToString($timestamp);
                        ?>
                        <div class="news-card">
                            <a class="news-card__img-wrap" href="<?= $rec['DETAIL_PAGE_URL']; ?>">
                                <? if (!empty($recImgSrc)): ?>
                                    <div class="news-card__img" style="background-image: url(<?= $recImgSrc ?>)"></div>
                                <? endif; ?>
                            </a>
                            <? if (!empty($rec['DETAIL_PAGE_URL']) && !empty($rec['NAME'])): ?>
                                <a class="news-card__title" href="<?= $rec['DETAIL_PAGE_URL']; ?>"><?= $rec['NAME']; ?></a>
                            <? endif; ?>
                            <? if (!empty($timeStampStr)): ?>
                                <div class="news-card__date"><?= $timeStampStr; ?></div>
                            <? endif; ?>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </section>
    <? endif; ?>
<? endif; ?>
