<?
use Letsrock\Lib\Models\PropertyHL;
$tagsProperty = 'TAGS';
/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();

// собираем id новостей
$foundItems = $arResult['SEARCH'];
$itemIdKey = 'ITEM_ID';

if (!empty($foundItems)) {
    $itemIds = array_map(function($item) {
        return $item['ITEM_ID'];
    }, $foundItems);


    //Достаем картинки и
    $infoblockItemsRes = CIBlockElement::GetList(
        [],
        ['IBLOCK_ID' => IB_NEWS_ID, 'ID' => $itemIds],
        false,
        false,
        ['ID', 'NAME', 'PREVIEW_PICTURE', 'PROPERTY_TAGS', 'PREVIEW_TEXT', 'DETAIL_PAGE_URL', 'DATE_CREATE']
    );
    $infoblockItemsRes->SetUrlTemplates('/news/#SECTION_CODE#/#ELEMENT_CODE#/');

    $infoblockItems = [];
    while($infoblockItem = $infoblockItemsRes->GetNext()) {
        $infoblockItems[] = $infoblockItem;
    }
    //собираем теги
    if (count($infoblockItems)) {
        $tagsCodes = [];
        $propertyTagsValue = 'PROPERTY_TAGS_VALUE';
        $propertyTagsName = 'PROPERTY_TAGS';

        foreach($infoblockItems as $item) {
            $itemTags = $item[$propertyTagsValue];
            if (!empty($itemTags)) {
                foreach ($itemTags as $itemTag) {
                    if (!in_array($itemTag, $tagsCodes)) $tagsCodes[] = $itemTag;
                }
            }
        }
        //Достаем теги из HL
        if (!empty($tagsCodes)) {
            $tagsFromHl = PropertyHL::getHlProperty(
                HL_TAGS_ID,
                ['UF_XML_ID' => $tagsCodes],
                ['ID', 'UF_NAME', 'UF_XML_ID']
            );
        }

        //присваиваем теги к результатам
        if (!empty($tagsFromHl)) {
            foreach($tagsFromHl as $tagFromHl) {
                foreach($infoblockItems as $resultItem) {
                    if (
                        is_array($resultItem[$propertyTagsValue])
                        && !empty($resultItem[$propertyTagsValue])
                        && in_array($tagFromHl['UF_XML_ID'], $resultItem[$propertyTagsValue])
                    ) {
                        $resultItem[$propertyTagsName][] = $tagFromHl;
                    }
                }
            }
        }
    }

    $arResult['SEARCH_ITEMS'] = $infoblockItems;
}
