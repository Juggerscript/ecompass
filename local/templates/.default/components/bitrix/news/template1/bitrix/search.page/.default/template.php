<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Letsrock\Lib\Models\Helper;
?>

<div class="news__toolbar">
    <?$APPLICATION->IncludeComponent(
        "bitrix:search.form",
        "flat",
        Array(
            "PAGE" => $arResult['URL']
        ),
        $component
    );?>
</div>

<? if (count($arResult["SEARCH_ITEMS"]) > 0): ?>
    <div class="news__list">
        <? foreach($arResult['SEARCH_ITEMS'] as $item):
            if ($item['PREVIEW_PICTURE']) {
                $imgSrc = Helper::getResizeImageArray($item['PREVIEW_PICTURE'], 'NEWS_LIST_PICTURE');
            }
            $name = $item['NAME'];
            $text = $item['PREVIEW_TEXT'];
            $link = $item['DETAIL_PAGE_URL'];
            $timestampStr = Helper::timestampXToString($item['DATE_CREATE']);
            ?>
            <div class="news-card">
                <a class="news-card__img-wrap" href="<?= $link ?>">
                    <? if (!empty($imgSrc)): ?>
                        <div class="news-card__img" style="background-image: url(<?= $imgSrc ?>)"></div>
                    <? endif; ?>
                </a>
                <? if (!empty($link) && !empty($name)): ?>
                    <a class="news-card__title" href="<?= $link ?>"><?= $name ?></a>
                <? endif; ?>
                <? if (!empty($timestampStr)): ?>
                    <div class="news-card__date"><?= $timestampStr ?></div>
                <? endif; ?>
            </div>
        <? endforeach; ?>
    </div>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?>
    <? endif; ?>

<? else: ?>
	<? ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND")); ?>
<? endif; ?>