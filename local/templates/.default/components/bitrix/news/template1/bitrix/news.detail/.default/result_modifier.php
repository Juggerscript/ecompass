<?
use Letsrock\Lib\Models\PropertyHL;
$tagsProperty = 'TAGS';
/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();

//Собираем теги
$tagsCodes = $arResult['PROPERTIES'][$tagsProperty]['VALUE'];
if (!empty($tagsCodes)) {
    $tags = PropertyHL::getHlProperties(HL_TAGS_ID, ['UF_XML_ID' => $tagsCodes]);
    $arResult[$tagsProperty] = $tags;
}
//собираем все новости
$allTheNews = CIBlockElement::GetList(
    [],
    ['IBLOCK_ID' => IB_NEWS_ID],
    false,
    false,
    ['ID', 'NAME', 'DETAIL_PAGE_URL', 'PREVIEW_PICTURE', 'TIMESTAMP_X', 'ACTIVE_FROM']
);
$allTheNews->SetUrlTemplates('/news/#SECTION_CODE#/#ELEMENT_CODE#/');
$news = [];
while($newsItem = $allTheNews->GetNext()) {
    $news[] = $newsItem;
}

//Находим предыдущую и следующую новость
$itemId = $arResult['ID'];
for ($i = 0; $i < count($news); $i ++) {
    if ($news[$i]['ID'] == $itemId) {
        if (isset($news[$i + 1])) {
            $arResult['NEXT_ITEM'] = $news[$i + 1];
        } else {
            $arResult['NEXT_ITEM'] = $news[0];
        }

        if (isset($news[$i - 1])) {
            $arResult['PREV_ITEM'] = $news[$i - 1];
        } else {
            $arResult['PREV_ITEM'] = $news[0];
        }
        break;
    }
}

//Собираем теги
$arResult['RECOMMENDATION'] = [];
$recommend = $arResult['PROPERTIES']['RECOMMENDATION']['VALUE'];
if (!empty($recommend)) {
    foreach($recommend as $recommendId) {
        foreach($news as $newsItem) {
            if ($recommendId == $newsItem['ID']) {
                $arResult['RECOMMENDATION'][] = $newsItem;
            }
        }
    }
}
?>

