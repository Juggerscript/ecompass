<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>

<div class="catalog__pagination">
    <section class="pagination">
        <div class="container">
            <ul class="pagination__list">

                <? while($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>

                    <? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
                        <li class="pagination__item">
                            <a class="active pagination__link" href="#"><?=$arResult["nStartPage"]?></a>
                        </li>
                    <? elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false): ?>
                        <li class="pagination__item">
                            <a class="pagination__link" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a>
                        </li>
                    <? else: ?>
                        <li class="pagination__item">
                            <a class="pagination__link"
                               href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"
                            ><?= $arResult["nStartPage"] ?></a>
                        </li>
                    <? endif ?>
                    <? $arResult["nStartPage"]++ ?>
                <? endwhile; ?>
            </ul>
        </div>
    </section>
</div>