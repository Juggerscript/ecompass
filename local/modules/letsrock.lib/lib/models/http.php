<?php

namespace Letsrock\Lib\Models;

use Bitrix\Main\Loader;
Loader::includeModule('iblock');


/*
 * Class Helper
 * Класс-помощник
 */

class Http
{
    public static function getFullUrl()
    {
        $url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        return static::addHttpsPrefix($url);
    }

    public static function addHttpsPrefix($string)
    {
        if (empty($string)) return false;
        $isHttps = $_SERVER['HTTPS'];

        $prefix = $isHttps ? 'https://' : 'http://';
        return $prefix . $string;
    }

    public static function getUrl($addLastSlash = true)
    {
        $slash = $addLastSlash ? '/' : '';
        $url = $_SERVER['HTTP_HOST'];
        $url = static::addHttpsPrefix($url);
        return $url . $slash;
    }
}