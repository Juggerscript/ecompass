<?php

namespace Letsrock\Lib\Models;

use Bitrix\Main\Loader;
use Bitrix\Highloadblock\HighloadBlockTable;
Loader::includeModule('highloadblock');

/*
 * Class propertyHL
 * Класс для работы с свойствами элемента инфоблока типа "справочник"
 */

class PropertyHL
{
    /** @param int $hlID */
    /** @param array $filter */
    public static function getHlProperty($hlID, $filter = [], $select = [])
    {
        if (empty($hlID)) die('highloadId is empty');

        $initSelect = ['*'];

        if (!empty($select)) {
            $initSelect = $select;
        }

        $arHLBlock = HighloadBlockTable::getById($hlID)->fetch();
        $obEntity = HighloadBlockTable::compileEntity($arHLBlock);
        $strEntityDataClass = $obEntity->getDataClass();
        $res = [];
        $resData = $strEntityDataClass::getList(array(
            'select' => $initSelect,
            'filter' => array($filter),
            'order'  => array('ID' => 'ASC'),
        ));
        if ($arItem = $resData->Fetch()) {
            $res[] = $arItem;
        }
        return $res;
    }

    /** @param int $hlID */
    /** @param array $filter */
    public static function getHlProperties($hlID, $filter = [], $select = [])
    {
        if (empty($hlID)) die('highloadId is empty');

        $initSelect = ['*'];

        if (!empty($select)) {
            $initSelect = $select;
        }

        $arHLBlock = HighloadBlockTable::getById($hlID)->fetch();
        $obEntity = HighloadBlockTable::compileEntity($arHLBlock);
        $strEntityDataClass = $obEntity->getDataClass();
        $res = [];
        $resData = $strEntityDataClass::getList(array(
            'select' => $initSelect,
            'filter' => array($filter),
            'order'  => array('ID' => 'ASC'),
        ));
        while ($arItem = $resData->Fetch()) {
            $res[] = $arItem;
        }
        return $res;
    }
}