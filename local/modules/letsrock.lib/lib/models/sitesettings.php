<?php

namespace Letsrock\Lib\Models;

use Bitrix\Main\Loader;

Loader::includeModule('iblock');

/**
 * Класс для работы с инфоблоком "Настройки сайта"
 * Class SiteSettings
 * @package Letsrock\Lib\Models
 */
class SiteSettings
{
    public static function getAddress()
    {
        return static::fetchElement(IB_SETTINGS_ADDRESS);

    }
    /** в PREVIEW_TEXT лежат конкретные координаты заведение, а в DETAIL_TEXT - размеры плейсмаркера */
    public static function getCoords()
    {
        return static::fetchElement(IB_SETTINGS_COORDINATES);
    }

    public static function getYandexMapPath()
    {
        return static::fetchElement(IB_SETTINGS_MAP_SCRIPT);
    }

    public static function getPhoneAndNumber()
    {
        return static::fetchElement(IB_SETTINGS_PHONE_EMAIL);
    }

//    public static function

    public static function getPreviewText($element)
    {
        $previewText = $element['PREVIEW_TEXT'];
        if (is_array($previewText) && key_exists('TEXT', $previewText)) {
            return $previewText['TEXT'];
        } else {
            return $previewText;
        }
    }

    public static function getDetailText($element)
    {
        $detailText = $element['DETAIL_TEXT'];
        if (is_array($detailText) && key_exists('TEXT', $detailText)) {
            return $detailText['TEXT'];
        } else {
            return $detailText;
        }
    }

    public static function getName($element)
    {
        return $element['NAME'];
    }

    public static function getPreviewPicture($element) {
        return $element['PREVIEW_PICTURE'];
    }

    private static function fetchElement($id) {
        return \CIBlockElement::GetById($id)->Fetch();
    }

    public static function getTelegramLink()
    {
        return static::getPreviewText(static::fetchElement(IB_SETTINGS_TELEGRAM_ELEMENT_ID));
    }

    public static function getInstagramLink()
    {
        return static::getPreviewText(static::fetchElement(IB_SETTINGS_INSTAGRAM_ELEMENT_ID));
    }

    public static function getFacebookLink()
    {
        return static::getPreviewText(static::fetchElement(IB_SETTINGS_FACEBOOK_ELEMENT_ID));
    }
}
