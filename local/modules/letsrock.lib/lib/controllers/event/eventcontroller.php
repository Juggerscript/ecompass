<?

namespace Letsrock\Lib\Controllers\Event;

abstract class EventController {
    protected $eventName;
    protected $result;

    abstract protected function send($data);
    abstract protected function getSuccess();
    abstract protected function getErrors();

    /**
     * @return mixed
     */
    public function getEventName()
    {
        return $this->eventName;
    }

    /**
     * @param mixed $eventName
     */
    public function setEventName($eventName): void
    {
        $this->eventName = $eventName;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result): void
    {
        $this->result = $result;
    }
}