<?

namespace Letsrock\Lib\Controllers\Event;
use \Bitrix\Main\Mail\Event;
use Letsrock\Lib\Models\JSONModel;

class CooperationRequestEventController extends EventController {
    public CONST USER_EMAIL = 'USER_EMAIL';
    public CONST USER_MESSAGE = 'USER_MESSAGE';
    public CONST USER_NAME = 'USER_NAME';
    public CONST REFERRER_PAGE = 'REFERRER_PAGE';
    public CONST USER_AGREE = 'USER_AGREE';
    public CONST USER_PHONE = 'USER_PHONE';
    public CONST USER_CITY = 'USER_CITY';

    protected $eventName = 'USER_COOPERATION_REQUEST';


    public function send($arFields)
    {
        if (
            empty($arFields[static::USER_NAME])
            || empty($arFields[static::USER_EMAIL])
            || empty($arFields[static::USER_AGREE])
            ) {
            $responseMessage = '';
            if (empty($arFields[static::USER_NAME])) $responseMessage .= 'Имя пользователя не заполнено <br>';
            if (empty($arFields[static::USER_EMAIL])) $responseMessage .= 'Почта пользователя не заполнена <br>';
            $responseModel = new JSONModel();
            $responseModel->setStatus(false);
            $responseModel->setData(['text' => $responseMessage]);
            http_response_code(200);
            die($responseModel->getAsString());
        }
        $this->setResult(Event::send([
            'EVENT_NAME' => $this->getEventName(),
            'LID' => 's1',
            'C_FIELDS' => $arFields,
        ]));
        $this->getSuccess();
    }

    protected function getSuccess()
    {
        if (!$this->getResult()->isSuccess()) $this->getErrors();

        $responseModel = new JSONModel();
        $responseModel->setStatus(true);
        $responseModel->setData(['text' => 'Заявка успешно отправлена, в скором времени с вами свяжется наш менеджер']);
        http_response_code(200);
        die($responseModel->getAsString());

    }

    protected function getErrors()
    {
        $result = $this->getResult();
        $responseModel = new JSONModel();
        $responseModel->setStatus(false);
        $responseModel->setData(['text' => $result->getErrors()->getMessage()]);
        http_response_code(200);
        die($responseModel->getAsString());
    }
}