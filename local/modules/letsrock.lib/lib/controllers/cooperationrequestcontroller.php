<?php

namespace Letsrock\Lib\Controllers;
use Letsrock\Lib\Controllers\Event\CooperationRequestEventController as CoopReqEvController;
/**
 * Контроллер для обработки заявок
 */
class CooperationRequestController {
    protected $eventController;

    public function __construct()
    {
        $this->setEventController(new CoopReqEvController());
    }

    public function sendRequest($request)
    {
        $arFields = [];
        $arFields[CoopReqEvController::USER_NAME] = htmlspecialcharsEx($request[CoopReqEvController::USER_NAME]);
        $arFields[CoopReqEvController::USER_EMAIL] = htmlspecialcharsEx($request[CoopReqEvController::USER_EMAIL]);
        $arFields[CoopReqEvController::USER_AGREE] = htmlspecialcharsEx($request[CoopReqEvController::USER_AGREE]);
        $arFields[CoopReqEvController::USER_PHONE] = htmlspecialcharsEx($request[CoopReqEvController::USER_PHONE]);

        if (!empty($request[CoopReqEvController::REFERRER_PAGE])) {
            $this->setReferrerPage($arFields, $request[CoopReqEvController::REFERRER_PAGE]);
        }

        if (!empty($request[CoopReqEvController::USER_MESSAGE])) {
            $arFields[CoopReqEvController::USER_MESSAGE] = htmlspecialcharsEx($request[CoopReqEvController::USER_MESSAGE]);
        }
        return $this->getEventController()->send($arFields);
    }

    /**
     * @return mixed
     */
    public function getEventController()
    {
        return $this->eventController;
    }

    /**
     * @param mixed $eventController
     */
    public function setEventController($eventController): void
    {
        $this->eventController = $eventController;
    }

    public function setReferrerPage(&$arFields, $referrerPage)
    {
        if (empty($arFields) || empty($referrerPage)) return false;
        $arFields[CoopReqEvController::REFERRER_PAGE] = $referrerPage;
        return true;
    }
}