<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Letsrock\Lib\Models\PropertyHL;
use Letsrock\Lib\Models\Helper;
$cases = PropertyHL::getHlProperties(HL_CASES_ID);
?>

<? if (!empty($cases)): ?>
    <section class="section cases-section">
        <div class="container">
            <h2 class="h2 section__title">Кейсы</h2>
        </div>
        <section class="cases-section__container swiper-container" data-controller="slider-cases">
            <div class="cases-section__list swiper-wrapper">
                <? foreach ($cases as $case):
                    if (!empty($case['UF_FILE'])) {
                        $imgSrc = Helper::getResizeImageArrayById($case['UF_FILE'], 'INDEX_CASES_IN_LIST');
                    }
                    $name = $case['UF_NAME'];
                    $description = $case['UF_DESCRIPTION'];
                    ?>
                    <div class="cases-section__item swiper-slide">
                        <div class="cases-section__img-wrap">
                            <img class="cases-section__img" src="<?= $imgSrc; ?>" alt="<?= $name; ?>">
                        </div>
                        <div class="cases-section__data">
                            <a class="cases-section__title" href="javascript:void(0)"
                               data-modal-open="<?= $case['UF_NAME']; ?>"><?= $name; ?></a>
                            <div class="cases-section__desc">
                                <?= $description; ?>
                            </div>
                            <div class="cases-section__bottom">
                                <a class="button button--mini"
                                   href="javascript:void(0)"
                                    data-modal-open="<?= $case['UF_NAME']; ?>">Подробнее</a>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </section>

        <section class="cases-section__modal-list" data-controller="modal">
            <? foreach ($cases as $case):
                if (empty($case['UF_FILE_FULL']) || empty($case['UF_TEXT_FULL'] || empty($case['UF_NAME']))) continue;
                $fullImgSrc = Helper::getResizeImageArrayById($case['UF_FILE_FULL'], 'INDEX_CASES_IN_LIST');
            ?>
                <div class="modal modal-slide modal--default modal--text" id="<?= $case['UF_NAME']; ?>">
                    <div class="modal__overlay" data-modal-close>
                        <div class="modal__container">
                            <div class="modal__header">
                                <h2 class="h2 modal__title"><?= $case['UF_NAME']; ?></h2>
                                <button class="modal__close" data-modal-close></button>
                            </div>
                            <div class="modal__content">
                                <div class="text">
                                    <?= $case['UF_TEXT_FULL']; ?>
                                    <img src="<?= $fullImgSrc; ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </section>
    </section>


<? endif; ?>
