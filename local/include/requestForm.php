<?
use Letsrock\Lib\Controllers\Event\CooperationRequestEventController as CoopReqEvController;
?>

<section class="section-half section form-section">
    <div class="container">
        <div class="section-half__inner">
            <div class="section-half__part">
                <h1 class="h2 form-section__title">Полный цикл для вашего бренда на всех маркетплейсах в одном окне</h1>
            </div>
            <div class="section-half__part">
                <form class="form"
                      method="post"
                      data-form-url="<?= AJAX_SEND_REQUEST; ?>"
                      data-action="submit->form#send"
                      data-form-event="success"
                      data-controller="form">
                    <div class="form-row">
                        <div class="form-half">
                            <label class="field field--transparent" data-controller="field" data-form-target="field">
                                <span class="field__placeholder">
                                    <span class="field__placeholder-text">Имя*</span>
                                </span>
                                <input class="field__input"
                                       name="<?= CoopReqEvController::USER_NAME; ?>"
                                       type="text"
                                       data-field-target="input"
                                       data-action="input->field#validate focus->field#addFocus blur->field#removeFocus"
                                       data-required
                                >
                            </label>
                        </div>
                        <div class="form-half">
                            <label class="field field--transparent" data-controller="field" data-form-target="field">
                                <span class="field__placeholder">
                                    <span class="field__placeholder-text">Электронная почта*</span>
                                </span>
                                <input class="field__input"
                                       name="<?= CoopReqEvController::USER_EMAIL; ?>"
                                       type="text"
                                       data-field-target="input"
                                       data-action="input->field#validate focus->field#addFocus blur->field#removeFocus"
                                       data-required>
                            </label>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="field field--textarea field--transparent" data-controller="field" data-form-target="field">
                            <span class="field__placeholder">
                                <span class="field__placeholder-text">Сообщение</span>
                            </span>
                            <textarea class="field__input field__textarea"
                                      name="<?= CoopReqEvController::USER_MESSAGE; ?>"
                                      data-field-target="input"
                                      data-action="input->field#validate focus->field#addFocus blur->field#removeFocus"></textarea>
                        </label>
                    </div>
                    <div class="form-policy">
                        <label class="field-checkbox form-agree field-checkbox--transparent">
                            <input class="field-checkbox__input"
                                   type="checkbox"
                                   name="<?= CoopReqEvController::USER_AGREE; ?>"
                                   checked
                                   data-required
                            >
                            <div class="field-checkbox__custom"></div>
                            <div class="field-checkbox__text">
                                Я согласен на обработку персональных данных
                            </div>
                        </label>
                    </div>
                    <div class="form-submit">
                        <input type="hidden" name="<?= CoopReqEvController::REFERRER_PAGE; ?>" value="<?= \Letsrock\Lib\Models\Http::getFullUrl(); ?>">
                        <button class="button button--with-icon" data-form-target="submit">
                            <i class="icon icon-mail button__icon"></i>
                            <span class="button__text">Оставить заявку</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
