<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?
    use Bitrix\Main\Page\Asset;

    CUtil::InitJSCore();
    CJSCore::Init(['fx', 'ajax', 'json', 'ls', 'session', 'jquery', 'popup', 'pull']);

    Asset::getInstance()->addString('<link rel="shortcut icon" href="' . DIST_SITE_DIR_PATH . 'img/favicons/favicon.ico" type="image/x-icon">');
    Asset::getInstance()->addString('<link rel="apple-touch-icon" href="' . DIST_SITE_DIR_PATH . 'img/favicons/apple-touch-icon.png">');
    Asset::getInstance()->addString('<link rel="apple-touch-icon" sizes="57x57" href="' . DIST_SITE_DIR_PATH . 'img/favicons/apple-touch-icon-57x57.png">');
    Asset::getInstance()->addString('<link rel="apple-touch-icon" sizes="72x72" href="' . DIST_SITE_DIR_PATH . 'img/favicons/apple-touch-icon-72x72.png">');
    Asset::getInstance()->addString('<link rel="apple-touch-icon" sizes="76x76" href="' . DIST_SITE_DIR_PATH . 'img/favicons/apple-touch-icon-76x76.png">');
    Asset::getInstance()->addString('<link rel="apple-touch-icon" sizes="114x114" href="' . DIST_SITE_DIR_PATH . 'img/favicons/apple-touch-icon-114x114.png">');
    Asset::getInstance()->addString('<link rel="apple-touch-icon" sizes="120x120" href="' . DIST_SITE_DIR_PATH . 'img/favicons/apple-touch-icon-120x120.png">');
    Asset::getInstance()->addString('<link rel="apple-touch-icon" sizes="144x144" href="' . DIST_SITE_DIR_PATH . 'img/favicons/apple-touch-icon-144x144.png">');
    Asset::getInstance()->addString('<link rel="apple-touch-icon" sizes="152x152" href="' . DIST_SITE_DIR_PATH . 'img/favicons/apple-touch-icon-152x152.png">');
    Asset::getInstance()->addString('<link rel="apple-touch-icon" sizes="180x180" href="' . DIST_SITE_DIR_PATH . 'img/favicons/apple-touch-icon-180x180.png">');

    //Подключаем статику
    Asset::getInstance()->addCss( DIST_SITE_DIR_PATH . "styles/critical.min.css");
    Asset::getInstance()->addCss( DIST_SITE_DIR_PATH . "styles/main.min.css");
    ?>
    <? $APPLICATION->ShowHead(); ?>
    <title><? $APPLICATION->ShowTitle(false)?></title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
</head>

<body>
<? $APPLICATION->ShowPanel(); ?>
<!-- wrapper open -->
<div class="wrapper">
    <!-- main-content open -->
    <div class="main-content">

        <header class="header" data-controller="header">
            <div class="header__inner">
                <a class="header__mobile-menu" href="javascript:void(0)" data-action="click-&gt;header#toggleMenu">
                    <i class="icon icon-mobile-menu"></i>
                </a>
                <a class="header__logo" href="/">
                    <svg class="logo" width="210" height="36" viewBox="0 0 210 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M20.0797 21.632H2.24486C2.55493 25.951 5.8292 28.1719 10.5422 28.1719C14.1017 28.1719 16.5698 26.9081 17.562 25.0799L19.6828 25.178C18.5294 28.0737 15.3792 29.8897 10.5298 29.8897C4.24167 29.8897 0 26.9572 0 20.9817C0 15.0308 4.3657 12.0737 10.2445 12.0737C16.3962 12.0737 20.0797 15.0676 20.0797 21.3007V21.632ZM2.28207 20.0369H17.8597C17.7356 16.1964 14.7714 13.7915 10.2445 13.7915C5.92842 13.7915 2.69135 15.902 2.28207 20.0369Z" fill="#0000A0"></path>
                        <path d="M31.9365 29.8897C25.6361 29.8897 21.5432 26.7731 21.5432 20.9817C21.5432 15.1903 25.6361 12.0737 31.8497 12.0737C36.6247 12.0737 40.6183 14.1228 41.3749 18.086L38.5595 18.2455C37.8154 15.7056 35.062 14.4418 31.8497 14.4418C27.5088 14.4418 24.5074 16.8835 24.5074 20.9817C24.5074 25.0799 27.5088 27.5093 31.9365 27.5093C35.2728 27.5093 37.9642 26.1105 38.7083 23.5093L41.6105 23.6688C40.7672 27.7056 36.9224 29.8897 31.9365 29.8897Z" fill="#0000A0"></path>
                        <path d="M53.3557 12.0737C59.6934 12.0737 63.9103 15.3621 63.9103 20.9817C63.9103 26.6013 59.6934 29.8897 53.3557 29.8897C47.018 29.8897 42.8011 26.6013 42.8011 20.9817C42.8011 15.3498 47.018 12.0737 53.3557 12.0737ZM53.3557 27.5093C58.0687 27.5093 60.9709 24.8713 60.9709 20.9817C60.9709 17.0799 58.0687 14.4418 53.3557 14.4418C48.6676 14.4418 45.7406 17.0799 45.7406 20.9817C45.7406 24.8835 48.6676 27.5093 53.3557 27.5093Z" fill="#0000A0"></path>
                        <path d="M98.898 19.3498V29.497H95.9586V20.1964C95.9586 16.7118 94.0858 14.6013 90.6875 14.6013C86.6939 14.6013 84.1265 17.3375 84.1265 21.8529V29.497H81.1623V20.1964C81.1623 16.7118 79.3144 14.6013 75.8912 14.6013C71.9224 14.6013 69.3303 17.3375 69.3303 21.8529V29.497H66.3661V12.4664H69.0823V16.8222C70.3597 13.8038 73.1379 12.0737 76.7966 12.0737C80.3562 12.0737 82.9731 13.7056 83.8165 16.9204C85.0939 13.8283 87.8721 12.0737 91.5557 12.0737C95.9586 12.0737 98.898 14.54 98.898 19.3498Z" fill="#0000A0"></path>
                        <path d="M123.033 20.9817C123.033 26.6013 119.003 29.8897 113.483 29.8897C110.333 29.8897 107.555 28.8467 105.93 26.6013V36.0001H101.279V12.4664H105.558V15.8774C107.121 13.2148 110.023 12.0737 113.421 12.0737C119.003 12.0737 123.033 15.3498 123.033 20.9817ZM118.444 20.9817C118.444 17.8651 116.014 15.7424 112.07 15.7424C108.101 15.7424 105.707 17.8529 105.707 20.9817C105.707 24.0983 108.113 26.221 112.07 26.221C116.014 26.2087 118.444 24.0983 118.444 20.9817Z" fill="#0000A0"></path>
                        <path d="M144.477 19.8037V29.497H140.174V25.8896C138.611 28.3314 135.746 29.8896 131.616 29.8896C126.494 29.8896 124.162 27.681 124.162 24.7853C124.162 21.2761 127.498 19.9142 133.377 19.5215L139.839 19.0921V18.7976C139.839 16.8467 138.338 15.3866 134.878 15.3866C132.137 15.3866 130.289 16.1718 129.755 17.7301H125.39C125.886 13.4725 130.165 12.0369 134.878 12.0737C141.017 12.0982 144.477 14.5768 144.477 19.8037ZM139.826 22.405V21.816L133.96 22.2455C130.624 22.4663 128.776 23.0921 128.776 24.5154C128.776 25.7179 130.09 26.5645 132.868 26.5645C137.234 26.5645 139.826 24.6136 139.826 22.405Z" fill="#0000A0"></path>
                        <path d="M155.292 15.3865C152.638 15.3865 151.485 16.0368 151.485 16.9816C151.485 17.8282 152.452 18.3804 153.978 18.6994L159.881 19.8405C163.689 20.5889 165.971 22.0245 165.971 24.7116C165.971 28.3804 162.002 29.8773 156.074 29.8773C150.418 29.8773 146.586 28.3558 146.114 24.319L150.418 24.184C150.79 25.7178 152.787 26.5521 156.471 26.5521C159.683 26.5521 161.308 26 161.308 24.9202C161.308 24.1104 160.34 23.5583 158.505 23.1656L152.266 21.9264C148.582 21.1779 146.834 19.4233 146.834 17.1166C146.834 13.3742 150.741 12.0491 155.54 12.0491C160.625 12.0491 164.842 13.546 165.4 17.411L161.097 17.5705C160.625 16.1963 158.418 15.3865 155.292 15.3865Z" fill="#0000A0"></path>
                        <path d="M175.992 15.3865C173.338 15.3865 172.185 16.0368 172.185 16.9816C172.185 17.8282 173.152 18.3804 174.677 18.6994L180.581 19.8405C184.389 20.5889 186.671 22.0245 186.671 24.7116C186.671 28.3804 182.702 29.8773 176.773 29.8773C171.118 29.8773 167.286 28.3558 166.814 24.319L171.118 24.184C171.49 25.7178 173.487 26.5521 177.17 26.5521C180.383 26.5521 182.007 26 182.007 24.9202C182.007 24.1104 181.04 23.5583 179.204 23.1656L172.966 21.9264C169.282 21.1779 167.534 19.4233 167.534 17.1166C167.534 13.3742 171.44 12.0491 176.24 12.0491C181.325 12.0491 185.542 13.546 186.1 17.411L181.796 17.5705C181.325 16.1963 179.118 15.3865 175.992 15.3865Z" fill="#0000A0"></path>
                        <path d="M210 11.0798L202.372 11.3006C200.276 11.3006 198.577 9.61963 198.577 7.54601L198.8 0H197.697L197.92 7.54601C197.92 9.61963 196.221 11.3006 194.125 11.3006L186.497 11.0798V12.1718L194.125 11.9509C196.221 11.9509 197.92 13.6319 197.92 15.7055L197.697 23.2515H198.8L198.577 15.7055C198.577 13.6319 200.276 11.9509 202.372 11.9509L210 12.1718V11.0798Z" fill="#0000A0"></path>
                    </svg>
                </a>

                <? $APPLICATION->IncludeComponent("bitrix:menu", "top", [
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "top",
                    "USE_EXT" => "N",
                ],
                    false
                ); ?>

                <div class="header__right">
                    <a class="header__modal-button" href="javascript:void(0)"
                       data-modal-open="feedback">
                        <i class="icon icon-mail header__modal-icon"></i>
                        <span class="header__modal-text">Оставить заявку</span>
                    </a>
                </div>

            </div>
        </header>
