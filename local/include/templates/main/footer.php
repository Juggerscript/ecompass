        <!-- main section end -->
        </main>
        <!-- marketplaces section -->
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "main.footer",
            Array(
                "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "N",
                "DETAIL_URL" => "#SITE_DIR#/marketplace-catalog/#CODE#/",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array("","={$arParams["LIST_FIELD_CODE"]}",""),
                "FILE_404" => $arParams["FILE_404"],
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => IB_MARKETPLACE_CATALOG_ID,
                "IBLOCK_TYPE" => "-",
                "IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => $arParams["MESSAGE_404"],
                "NEWS_COUNT" => 6,
                "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                "PARENT_SECTION" => $arResult["VARIABLES"]["SECTION_ID"],
                "PARENT_SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
                "PROPERTY_CODE" => array("","={['PROPERTY_VISITORS_PER_MONTH','PROPERTY_OFFERS_PER_DAY']}",""),
                "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                "SET_BROWSER_TITLE" => "Y",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "Y",
                "SET_META_KEYWORDS" => "Y",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => $arParams["SORT_BY1"],
                "SORT_BY2" => $arParams["SORT_BY2"],
                "SORT_ORDER1" => $arParams["SORT_ORDER1"],
                "SORT_ORDER2" => $arParams["SORT_ORDER2"],
                "STRICT_SECTION_CHECK" => "N",
                "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"]
            ),
            $component
        );?>

<!--        --><?// if ($APPLICATION->GetCurPage() == SITE_DIR): ?>
<!--            --><?// require(INCLUDE_PATH . 'cases.php') ?>
<!--        --><?// endif; ?>

        <? require(INCLUDE_PATH . 'requestForm.php') ?>
<!--        --><?// require(INCLUDE_PATH . 'partners.php') ?>
    <? require(COMMON_TEMPLATES_PATH . 'footer.php'); ?>
