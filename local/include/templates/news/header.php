<? require(COMMON_TEMPLATES_PATH . 'header.php'); ?>

        <main class="inner-page">
            <?$APPLICATION->IncludeComponent("bitrix:breadcrumb","",Array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            );?>

            <section class="section-title">
                <div class="container">
                    <h1 class="h1 section__h1"><?= $APPLICATION->ShowTitle(); ?></h1>
                </div>
            </section>
