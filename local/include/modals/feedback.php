<?
use Letsrock\Lib\Controllers\Event\CooperationRequestEventController as CoopReqEvController;
?>

<div class="modal modal-slide modal--feedback" id="feedback">
    <div class="modal__overlay" data-modal-close>
        <div class="modal__container">
            <div class="modal__header">
                <h2 class="h2 modal__title">Оставить заявку</h2>
                <button class="modal__close" data-modal-close></button>
            </div>
            <div class="modal__content">
                <form class="form" method="post"
                      data-controller="form"
                      data-form-url="<?= AJAX_SEND_REQUEST; ?>"
                      data-form-event="success"
                      data-action="submit-&gt;form#send">
                    <div class="form-row">
                        <div class="form-half">
                            <label class="field" data-controller="field" data-form-target="field">
                                <span class="field__placeholder">
                                    <span class="field__placeholder-text">Контактное лицо*</span>
                                </span>
                                <input class="field__input"
                                       name="<?= CoopReqEvController::USER_NAME; ?>" type="text"
                                       data-field-target="input"
                                       data-action="input-&gt;field#validate focus-&gt;field#addFocus blur-&gt;field#removeFocus"
                                       data-required>
                            </label>
                        </div>
                        <div class="form-half">
                            <label class="field" data-controller="field" data-form-target="field">
                                <span class="field__placeholder">
                                    <span class="field__placeholder-text">Город</span>
                                </span>
                                <input class="field__input"
                                       name="<?= CoopReqEvController::USER_CITY; ?>" type="text"
                                       data-field-target="input"
                                       data-action="input-&gt;field#validate focus-&gt;field#addFocus blur-&gt;field#removeFocus">
                            </label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-half">
                            <label class="field" data-controller="field" data-form-target="field">
                                <span class="field__placeholder">
                                    <span class="field__placeholder-text">Email*</span>
                                </span>
                                <input class="field__input"
                                       name="<?= CoopReqEvController::USER_EMAIL; ?>" type="text"
                                       data-field-target="input"
                                       data-action="input-&gt;field#validate focus-&gt;field#addFocus blur-&gt;field#removeFocus"
                                       data-validate="email"
                                       data-required>
                            </label>
                        </div>
                        <div class="form-half">
                            <label class="field" data-controller="field" data-form-target="field">
                                <span class="field__placeholder">
                                    <span class="field__placeholder-text">Телефон*</span>
                                </span>
                                <input class="field__input"
                                       name="<?= CoopReqEvController::USER_PHONE; ?>" type="text"
                                       data-field-target="input"
                                       data-action="input-&gt;field#validate focus-&gt;field#addFocus blur-&gt;field#removeFocus"
                                       data-mask="phone" data-validate="phone"
                                       data-required>
                            </label>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="field field--textarea" data-controller="field" data-form-target="field">
                            <span class="field__placeholder">
                                <span class="field__placeholder-text">Сообщение</span>
                            </span>
                            <textarea class="field__input field__textarea"
                                      name="<?= CoopReqEvController::USER_MESSAGE; ?>" data-field-target="input"
                                      data-action="input-&gt;field#validate focus-&gt;field#addFocus blur-&gt;field#removeFocus"
                            ></textarea>
                        </label>
                    </div>
                    <div class="form-policy">
                        <label class="field-checkbox form-agree">
                            <input class="field-checkbox__input"
                                   type="checkbox"
                                   name="<?= CoopReqEvController::USER_AGREE; ?>"
                                   checked
                                   data-required>
                            <div class="field-checkbox__custom"></div>
                            <div class="field-checkbox__text">Я согласен на обработку персональных данных</div>
                        </label>
                    </div>
                    <div class="form-submit">
                        <input type="hidden" name="<?= CoopReqEvController::REFERRER_PAGE; ?>" value="<?= \Letsrock\Lib\Models\Http::getFullUrl(); ?>">
                        <button class="button button--submit" type="submit" data-target="form.submit">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
