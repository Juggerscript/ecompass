<?
use Letsrock\Lib\Models\PropertyHL;
use Letsrock\Lib\Models\Helper;

$partnersCount = 12;

$partners = PropertyHL::getHlProperty(HL_PARTNERS_ID, [], ['UF_NAME', 'UF_FILE']);

?>
<? if (!empty($partners)): ?>
    <section class="section brands">
        <div class="container">
            <h2 class="h2 section__title">С нами уже работают</h2>
        </div>
        <div class="container">
            <div class="section__inner">
                <div class="brands__list">
                    <? foreach ($partners as $partner):
                        $imgSrc = Helper::getResizeImageArrayById($partner['UF_FILE'], 'PARTNERS_CARDS_ON_LIST'); ?>
                        <? if (!empty($imgSrc)): ?>
                            <div class="brands__item">
                                <img class="brands__img" src="<?= $imgSrc; ?>" alt="">
                            </div>
                        <? endif; ?>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </section>
<? endif; ?>
