/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/js/index.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/blocks/modules/footer/footer.js":
/*!*********************************************!*\
  !*** ./src/blocks/modules/footer/footer.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./src/blocks/modules/header/header.js":
/*!*********************************************!*\
  !*** ./src/blocks/modules/header/header.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./src/js/classes/HTTP.js":
/*!********************************!*\
  !*** ./src/js/classes/HTTP.js ***!
  \********************************/
/*! exports provided: default, isProd */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HTTP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isProd", function() { return isProd; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var query_string__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! query-string */ "./node_modules/query-string/index.js");
/* harmony import */ var query_string__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(query_string__WEBPACK_IMPORTED_MODULE_4__);






var HTTP = /*#__PURE__*/function () {
  function HTTP(url, body) {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default()(this, HTTP);

    this.url = url;
    this.body = body;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default()(HTTP, [{
    key: "request",
    value: function () {
      var _request = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(method) {
        var url, options;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                url = this.url;
                options = {
                  method: method,
                  credentials: 'include',
                  // Чтобы браузеры могли отправлять запрос с учётными данными (даже для cross-origin запросов)
                  headers: {
                    'Content-Type': 'application/json' // 'Content-Type': 'application/x-www-form-urlencoded',

                  }
                };

                if (method === 'get') {
                  url = "".concat(url, "?").concat(query_string__WEBPACK_IMPORTED_MODULE_4___default.a.stringify(this.body));
                } else {
                  options.body = JSON.stringify(this.body);
                }

                _context.next = 5;
                return fetch(url, options).then(function (response) {
                  if (response.ok) {
                    return response.json();
                  }

                  return Promise.reject(response);
                }).catch(function (error) {
                  console.warn(error);
                });

              case 5:
                return _context.abrupt("return", _context.sent);

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function request(_x) {
        return _request.apply(this, arguments);
      }

      return request;
    }()
  }, {
    key: "get",
    value: function get() {
      return this.request('get');
    }
  }, {
    key: "post",
    value: function post() {
      return this.request(isProd ? 'post' : 'get');
    }
  }]);

  return HTTP;
}();


var isProd = "development" === 'production';

/***/ }),

/***/ "./src/js/classes/Mask.js":
/*!********************************!*\
  !*** ./src/js/classes/Mask.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Mask; });
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var inputmask__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! inputmask */ "./node_modules/inputmask/index.js");
/* harmony import */ var inputmask__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(inputmask__WEBPACK_IMPORTED_MODULE_3__);




function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }



var Mask = /*#__PURE__*/function () {
  function Mask(selector) {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, Mask);

    this.selector = selector;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(Mask, [{
    key: "init",
    value: function init() {
      new Mask('.js-mask-phone').phone();
    }
  }, {
    key: "phone",
    value: function phone(options) {
      return new inputmask__WEBPACK_IMPORTED_MODULE_3___default.a(_objectSpread(_objectSpread({}, options), {}, {
        mask: '+7 (999) 999-99-99',
        showMaskOnHover: false,
        autoUnmask: true
      })).mask(this.selector);
    }
  }]);

  return Mask;
}();



/***/ }),

/***/ "./src/js/classes/Modal.js":
/*!*********************************!*\
  !*** ./src/js/classes/Modal.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _default; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var micromodal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! micromodal */ "./node_modules/micromodal/dist/micromodal.es.js");





var _default = /*#__PURE__*/function () {
  function _default() {
    var _this = this;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, _default);

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_2___default()(this, "toggleHidden", function (flag) {
      var body = _this.body;
      var scrollbarWidth = window.innerWidth - body.clientWidth;

      if (flag) {
        body.classList.add('o-hidden');
        body.style.marginRight = "".concat(scrollbarWidth, "px");
      } else {
        body.classList.remove('o-hidden');
        body.style.marginRight = 'auto';
      }
    });
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(_default, [{
    key: "init",
    value: function init() {
      micromodal__WEBPACK_IMPORTED_MODULE_3__["default"].init(this.options);
    }
  }, {
    key: "show",
    value: function show(id) {
      micromodal__WEBPACK_IMPORTED_MODULE_3__["default"].show(id, this.options);
    }
  }, {
    key: "close",
    value: function close(id) {
      micromodal__WEBPACK_IMPORTED_MODULE_3__["default"].close(id);
    }
  }, {
    key: "closeAll",
    value: function closeAll() {
      var body = this.body;
      if (!body) return;
      body.querySelectorAll('.modal.is-open').forEach(function (i) {
        return micromodal__WEBPACK_IMPORTED_MODULE_3__["default"].close(i.id);
      });
    }
  }, {
    key: "body",
    get: function get() {
      return document.querySelector('body');
    }
  }, {
    key: "openTrigger",
    get: function get() {
      return 'data-modal-open';
    }
  }, {
    key: "closeTrigger",
    get: function get() {
      return 'data-modal-close';
    }
  }, {
    key: "options",
    get: function get() {
      var _this2 = this;

      return {
        openTrigger: this.openTrigger,
        closeTrigger: this.closeTrigger,
        awaitCloseAnimation: true,
        onShow: function onShow() {
          return _this2.toggleHidden(true);
        },
        onClose: function onClose() {
          return _this2.toggleHidden(false);
        }
      };
    }
  }]);

  return _default;
}();



/***/ }),

/***/ "./src/js/common/helpers.js":
/*!**********************************!*\
  !*** ./src/js/common/helpers.js ***!
  \**********************************/
/*! exports provided: checkEmail, checkPhone */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "checkEmail", function() { return checkEmail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "checkPhone", function() { return checkPhone; });
var checkEmail = function checkEmail(string) {
  return !!string.search(/^[-._a-z0-9]+@+[a-z0-9-]+\.[a-z]{2,6}$/i);
};
var checkPhone = function checkPhone(string) {
  return !!string.search(/^\+7\s\(([0-9]{3})\)\s([0-9]{3})-([0-9]{2})-([0-9]{2})$/i);
};

/***/ }),

/***/ "./src/js/common/slideAnimate.js":
/*!***************************************!*\
  !*** ./src/js/common/slideAnimate.js ***!
  \***************************************/
/*! exports provided: slideUp, slideDown, slideToggle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideUp", function() { return slideUp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideDown", function() { return slideDown; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideToggle", function() { return slideToggle; });
function delay(ms) {
  return new Promise(function (resolve, reject) {
    setTimeout(resolve, ms);
  });
}

var slideUp = function slideUp(target) {
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
  target.style.transitionProperty = 'height, margin, padding';
  target.style.transitionDuration = duration + 'ms';
  target.style.boxSizing = 'border-box';
  target.style.height = target.offsetHeight + 'px';
  target.offsetHeight;
  target.style.overflow = 'hidden';
  target.style.height = 0;
  target.style.paddingTop = 0;
  target.style.paddingBottom = 0;
  target.style.marginTop = 0;
  target.style.marginBottom = 0;
  return delay(duration).then(function () {
    target.style.display = 'none';
    target.style.removeProperty('height');
    target.style.removeProperty('padding-top');
    target.style.removeProperty('padding-bottom');
    target.style.removeProperty('margin-top');
    target.style.removeProperty('margin-bottom');
    target.style.removeProperty('overflow');
    target.style.removeProperty('transition-duration');
    target.style.removeProperty('transition-property');
  });
};
var slideDown = function slideDown(target) {
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
  target.style.removeProperty('display');
  var display = window.getComputedStyle(target).display;
  if (display === 'none') display = 'block';
  target.style.display = display;
  var height = target.offsetHeight;
  target.style.overflow = 'hidden';
  target.style.height = 0;
  target.style.paddingTop = 0;
  target.style.paddingBottom = 0;
  target.style.marginTop = 0;
  target.style.marginBottom = 0;
  target.offsetHeight;
  target.style.boxSizing = 'border-box';
  target.style.transitionProperty = 'height, margin, padding';
  target.style.transitionDuration = duration + 'ms';
  target.style.height = height + 'px';
  target.style.removeProperty('padding-top');
  target.style.removeProperty('padding-bottom');
  target.style.removeProperty('margin-top');
  target.style.removeProperty('margin-bottom');
  return delay(duration).then(function () {
    target.style.removeProperty('height');
    target.style.removeProperty('overflow');
    target.style.removeProperty('transition-duration');
    target.style.removeProperty('transition-property');
  });
};
var slideToggle = function slideToggle(target) {
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;

  if (window.getComputedStyle(target).display === 'none') {
    return slideDown(target, duration);
  } else {
    return slideUp(target, duration);
  }
};

/***/ }),

/***/ "./src/js/controllers sync recursive \\.js$":
/*!***************************************!*\
  !*** ./src/js/controllers sync \.js$ ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./dd_controller.js": "./src/js/controllers/dd_controller.js",
	"./field_controller.js": "./src/js/controllers/field_controller.js",
	"./filter_controller.js": "./src/js/controllers/filter_controller.js",
	"./form_controller.js": "./src/js/controllers/form_controller.js",
	"./header_controller.js": "./src/js/controllers/header_controller.js",
	"./map_controller.js": "./src/js/controllers/map_controller.js",
	"./modal_controller.js": "./src/js/controllers/modal_controller.js",
	"./share_controller.js": "./src/js/controllers/share_controller.js",
	"./slider-cases_controller.js": "./src/js/controllers/slider-cases_controller.js",
	"./slider-logistic_controller.js": "./src/js/controllers/slider-logistic_controller.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./src/js/controllers sync recursive \\.js$";

/***/ }),

/***/ "./src/js/controllers/dd_controller.js":
/*!*********************************************!*\
  !*** ./src/js/controllers/dd_controller.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _default; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
/* harmony import */ var _common_slideAnimate__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! %common%/slideAnimate */ "./src/js/common/slideAnimate.js");







function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }




var _default = /*#__PURE__*/function (_Controller) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(_default, _Controller);

  var _super = _createSuper(_default);

  function _default() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, _default);

    return _super.apply(this, arguments);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(_default, [{
    key: "toggle",
    value: function toggle(e) {
      e.preventDefault();

      if (this.isMobileOnly) {
        return;
      }

      var classList = this.element.classList;
      var isActive = classList.contains('active');
      var list = this.list;

      if (isActive) {
        Object(_common_slideAnimate__WEBPACK_IMPORTED_MODULE_7__["slideUp"])(list).then(function () {
          return classList.remove('active');
        });
      } else {
        Object(_common_slideAnimate__WEBPACK_IMPORTED_MODULE_7__["slideDown"])(list).then(function () {
          return classList.add('active');
        });
      }
    }
  }, {
    key: "list",
    get: function get() {
      return this.listTarget;
    }
  }, {
    key: "isMobileOnly",
    get: function get() {
      return this.data.has('is-mobile-only') && window.matchMedia('(min-width: 768px)').matches;
    }
  }]);

  return _default;
}(stimulus__WEBPACK_IMPORTED_MODULE_6__["Controller"]);

_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default()(_default, "targets", ['list']);



/***/ }),

/***/ "./src/js/controllers/field_controller.js":
/*!************************************************!*\
  !*** ./src/js/controllers/field_controller.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _default; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
/* harmony import */ var _common_helpers__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! %common%/helpers */ "./src/js/common/helpers.js");
/* harmony import */ var _classes_Mask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! %classes%/Mask */ "./src/js/classes/Mask.js");







function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }





var _default = /*#__PURE__*/function (_Controller) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(_default, _Controller);

  var _super = _createSuper(_default);

  function _default() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, _default);

    return _super.apply(this, arguments);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(_default, [{
    key: "getField",
    value: function getField(node) {
      return node.closest(this.selectorField);
    }
    /**
     * Action
     * @param e
     */

  }, {
    key: "addFocus",
    value: function addFocus(e) {
      e.preventDefault();
      var field = this.getField(e.target);
      field.classList.add(this.classFocus);
    }
    /**
     * Action
     * @param e
     */

  }, {
    key: "removeFocus",
    value: function removeFocus(e) {
      e.preventDefault();
      var target = e.target;
      if (target.value) return;
      var field = this.getField(target);
      field.classList.remove(this.classFocus);
    }
    /**
     * Action
     * @param e
     */

  }, {
    key: "toggleShowPassword",
    value: function toggleShowPassword(e) {
      e.preventDefault();
      var target = e.target;
      var input = this.input;
      var type = input.getAttribute('type');
      input.setAttribute('type', type === 'password' ? 'text' : 'password');
      target.classList.toggle('off');
    }
    /**
     * Action
     * @param target
     */

  }, {
    key: "validate",
    value: function validate(_ref) {
      var target = _ref.target;
      var value = typeof target.value === 'string' ? target.value.trim() : target.value || '';
      var message = '';

      if (this.isRequired) {
        message = !value.length ? this.errorMessage.required : '';
      }

      if (value.length) {
        /* eslint-disable */
        switch (this.typeValidate) {
          case 'email':
            message = Object(_common_helpers__WEBPACK_IMPORTED_MODULE_7__["checkEmail"])(value) ? this.errorMessage.email : '';
            break;

          case 'phone':
            var valuePhone = target.inputmask ? target.inputmask._valueGet() : value;
            message = Object(_common_helpers__WEBPACK_IMPORTED_MODULE_7__["checkPhone"])(valuePhone) ? this.errorMessage.phone : '';
            break;

          default:
            break;
        }
        /* eslint-enable */

      }

      this.error = message;
    }
    /**
     * Lifecycle
     */

  }, {
    key: "connect",
    value: function connect() {
      this.element[this.identifier] = this;

      if (this.input.value) {
        this.element.classList.add(this.classFocus);
      }

      if (this.typeMask === 'phone') {
        new _classes_Mask__WEBPACK_IMPORTED_MODULE_8__["default"](this.input).phone();
      }
    }
  }, {
    key: "classFocus",
    get: function get() {
      return 'focus';
    }
  }, {
    key: "selectorField",
    get: function get() {
      return ".".concat(this.element.classList[0]);
    }
  }, {
    key: "input",
    get: function get() {
      return this.inputTarget;
    }
  }, {
    key: "isRequired",
    get: function get() {
      return 'required' in this.input.dataset;
    }
  }, {
    key: "typeValidate",
    get: function get() {
      return this.input.dataset.validate;
    }
  }, {
    key: "typeMask",
    get: function get() {
      return this.input.dataset.mask;
    }
  }, {
    key: "error",
    set: function set(message) {
      var valid = !message.length;

      if (!valid) {
        this.element.classList.add('error');
        this.data.set('error', message);
      } else {
        this.element.classList.remove('error');
        delete this.element.dataset.fieldError;
      }

      document.dispatchEvent(new CustomEvent('field.valid', {
        detail: {
          valid: valid,
          message: message
        }
      }));
    }
  }, {
    key: "errorMessage",
    get: function get() {
      return {
        required: 'Обязательное поле',
        email: 'Почта введена некорректно',
        phone: 'Телефон введен некорректно'
      };
    }
  }]);

  return _default;
}(stimulus__WEBPACK_IMPORTED_MODULE_6__["Controller"]);

_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default()(_default, "targets", ['input']);



/***/ }),

/***/ "./src/js/controllers/filter_controller.js":
/*!*************************************************!*\
  !*** ./src/js/controllers/filter_controller.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _default; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");







function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }



var _default = /*#__PURE__*/function (_Controller) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(_default, _Controller);

  var _super = _createSuper(_default);

  function _default() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, _default);

    return _super.apply(this, arguments);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(_default, [{
    key: "processDataProps",
    value: function processDataProps(props) {
      return props.reduce(function (t, i) {
        var name = i.getAttribute('name');
        var type = i.getAttribute('type');
        /* eslint-disable */

        switch (type) {
          case 'checkbox':
            if (i.checked) {
              t[name] = i.checked;
            }

            break;

          default:
            if (i.value) {
              t[name] = i.value;
            }

        }
        /* eslint-enable */


        return t;
      }, {});
    }
  }, {
    key: "propsNodeByItem",
    value: function propsNodeByItem(target) {
      return this.processDataProps(this.propTargets.filter(function (i) {
        return target.contains(i);
      }));
    }
  }, {
    key: "getFilterItemByChild",
    value: function getFilterItemByChild(target) {
      return target.closest(this.selectorItem);
    }
  }, {
    key: "getItemResultElement",
    value: function getItemResultElement(target) {
      return this.resultTargets.filter(function (i) {
        return target.contains(i);
      })[0];
    }
    /**
     * Action
     * @param e
     */

  }, {
    key: "open",
    value: function open(e) {
      e.preventDefault();
      var target = this.getFilterItemByChild(e.target);

      if (target && target.classList.contains(this.classActive)) {
        this.closeAll();
      } else {
        this.closeAll();
        target.classList.add(this.classActive);
      }
    }
    /**
     * Action
     * @param e
     */

  }, {
    key: "apply",
    value: function apply(e) {
      e.preventDefault();
      var target = this.getFilterItemByChild(e.target);
      var resultEl = this.getItemResultElement(target);
      var props = this.propsNodeByItem(target);
      target.classList.add('selected');
      resultEl.innerText = Object.values(props).length; // TODO:: need send to server?
    }
    /**
     * Action
     * @param e
     */

  }, {
    key: "reset",
    value: function reset(e) {
      e.preventDefault();
      var target = this.getFilterItemByChild(e.target);
      target.classList.remove('selected');
      var resultEl = this.getItemResultElement(target);
      resultEl.innerText = '';
      this.closeAll();
    }
    /**
     * Inner method
     */

  }, {
    key: "closeAll",
    value: function closeAll() {
      var _this = this;

      this.itemTargets.map(function (i) {
        return i.classList.remove(_this.classActive);
      });
    }
    /**
     * Lifecycle
     */

  }, {
    key: "initialize",
    value: function initialize() {
      var _this2 = this;

      document.addEventListener('click', function (_ref) {
        var target = _ref.target;

        if (_this2.element !== target.closest(_this2.selectorElement)) {
          _this2.closeAll();
        }
      });
    }
  }, {
    key: "classActive",
    get: function get() {
      return this.data.get('classActive') || 'active';
    }
  }, {
    key: "selectorItem",
    get: function get() {
      return ".".concat(this.itemTarget.classList[0]);
    }
  }, {
    key: "selectorElement",
    get: function get() {
      return ".".concat(this.element.classList[0]);
    }
  }, {
    key: "documentHtml",
    get: function get() {
      return document.querySelector('html');
    }
  }]);

  return _default;
}(stimulus__WEBPACK_IMPORTED_MODULE_6__["Controller"]);

_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default()(_default, "targets", ['select', 'item', 'prop', 'result']);



/***/ }),

/***/ "./src/js/controllers/form_controller.js":
/*!***********************************************!*\
  !*** ./src/js/controllers/form_controller.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _default; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
/* harmony import */ var _classes_HTTP__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! %classes%/HTTP */ "./src/js/classes/HTTP.js");
/* harmony import */ var _classes_Modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! %classes%/Modal */ "./src/js/classes/Modal.js");







function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }





var _default = /*#__PURE__*/function (_Controller) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(_default, _Controller);

  var _super = _createSuper(_default);

  function _default() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, _default);

    return _super.apply(this, arguments);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(_default, [{
    key: "initialize",
    value: function initialize() {
      this.modal = new _classes_Modal__WEBPACK_IMPORTED_MODULE_8__["default"]();
    }
  }, {
    key: "connect",
    value: function connect() {
      this.listenValidate();
    }
  }, {
    key: "send",
    value: function send(e) {
      var _this = this;

      e.preventDefault();
      this.validate();
      this.processAgree();

      if (!this.hasError && this.isAgree) {
        this.showLoader();
        new _classes_HTTP__WEBPACK_IMPORTED_MODULE_7__["default"](this.url, Object.fromEntries(new FormData(this.element))).request(this.method).then(function (res) {
          console.log(res);

          _this.hideLoader();

          _this.handlerSuccess(res);
        });
      }
    }
  }, {
    key: "processAgree",
    value: function processAgree() {
      if (this.isAgree) {
        this.submit.removeAttribute('disabled');

        if (this.hasAgree) {
          this.agree.classList.remove('error');
        }
      } else {
        this.submit.setAttribute('disabled', true);

        if (this.hasAgree) {
          this.agree.classList.add('error');
        }
      }
    }
  }, {
    key: "validate",
    value: function validate() {
      this.fields.map(function (i) {
        return i.field.validate({
          target: i.field.input
        });
      });
    }
  }, {
    key: "listenValidate",
    value: function listenValidate() {
      var _this2 = this;

      document.addEventListener('field.valid', function () {
        if (_this2.hasError) {
          _this2.submit.setAttribute('disabled', true);
        } else {
          _this2.submit.removeAttribute('disabled');
        }
      });
    }
  }, {
    key: "buttonLoader",
    value: function buttonLoader() {
      var spin = document.createElement('div');
      spin.classList.add('ld', 'ld-ring', 'ld-spin');
      return spin;
    }
  }, {
    key: "showLoader",
    value: function showLoader() {
      this.submit.classList.add('ld-ext-right', 'running');
      this.submit.appendChild(this.buttonLoader());
    }
  }, {
    key: "hideLoader",
    value: function hideLoader() {
      this.submit.classList.remove('ld-ext-right', 'running');
      var spin = this.submit.querySelector('.ld');

      if (spin) {
        spin.remove();
      }
    }
  }, {
    key: "handlerSuccess",
    value: function handlerSuccess() {
      this.modal.closeAll();

      if (this.event) {
        this.modal.show(this.event);
      }

      if (this.redirect) {
        location.href = this.redirect;
      }
    }
  }, {
    key: "handlerAgree",
    value: function handlerAgree() {
      this.processAgree();
    }
  }, {
    key: "url",
    get: function get() {
      return this.data.get('url');
    }
  }, {
    key: "event",
    get: function get() {
      return this.data.get('event');
    }
  }, {
    key: "redirect",
    get: function get() {
      return this.data.get('redirect');
    }
  }, {
    key: "method",
    get: function get() {
      return this.data.get('method') || this.element.getAttribute('method') || 'post';
    }
  }, {
    key: "fields",
    get: function get() {
      return this.fieldTargets;
    }
  }, {
    key: "hasAgree",
    get: function get() {
      return this.hasAgreeTarget;
    }
  }, {
    key: "agree",
    get: function get() {
      return this.hasAgree ? this.agreeTarget : false;
    }
  }, {
    key: "isAgree",
    get: function get() {
      return this.agree ? this.agree.checked : true;
    }
  }, {
    key: "submit",
    get: function get() {
      return this.submitTarget;
    }
  }, {
    key: "hasError",
    get: function get() {
      return !!this.fields.reduce(function (t, i) {
        t.push(i.classList.contains('error'));
        return t;
      }, []).filter(function (i) {
        return i;
      }).length;
    }
  }]);

  return _default;
}(stimulus__WEBPACK_IMPORTED_MODULE_6__["Controller"]);

_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default()(_default, "targets", ['field', 'submit', 'agree']);



/***/ }),

/***/ "./src/js/controllers/header_controller.js":
/*!*************************************************!*\
  !*** ./src/js/controllers/header_controller.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _default; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }



var _default = /*#__PURE__*/function (_Controller) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(_default, _Controller);

  var _super = _createSuper(_default);

  function _default() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, _default);

    return _super.apply(this, arguments);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(_default, [{
    key: "toggleMenu",
    value: function toggleMenu(e) {
      e.preventDefault();
      this.element.classList.toggle('active');
      this.body.classList.toggle('o-hidden');
    }
  }, {
    key: "body",
    get: function get() {
      return document.querySelector('body');
    }
  }]);

  return _default;
}(stimulus__WEBPACK_IMPORTED_MODULE_5__["Controller"]);



/***/ }),

/***/ "./src/js/controllers/map_controller.js":
/*!**********************************************!*\
  !*** ./src/js/controllers/map_controller.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _default; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");







function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

/* global ymaps */


var _default = /*#__PURE__*/function (_Controller) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(_default, _Controller);

  var _super = _createSuper(_default);

  function _default() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, _default);

    return _super.apply(this, arguments);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(_default, [{
    key: "itemData",
    value: function itemData(el) {
      return {
        geoX: el.dataset['geoX'],
        geoY: el.dataset['geoY'],
        hint: el.dataset['hint']
      };
    }
  }, {
    key: "pick",
    value: function pick(_ref) {
      var target = _ref.target;
      var data = this.itemData(target);
      this.mapObject.setCenter([data.geoX, data.geoY]);
      this.mapObject.setZoom(16);
    }
    /**
     * Lifecycle
     */

  }, {
    key: "connect",
    value: function connect() {
      this.initScript();
    }
  }, {
    key: "disconnect",
    value: function disconnect() {
      this.destroyMap();
    }
  }, {
    key: "destroyMap",
    value: function destroyMap() {
      if (this.mapObject) {
        this.mapObject.destroy();
      }
    }
  }, {
    key: "isMobile",
    value: function isMobile() {
      return window.innerWidth <= 992;
    }
  }, {
    key: "initScript",
    value: function initScript() {
      var _this = this;

      var id = 'ymaps-component';

      if (!document.getElementById(id)) {
        var script = document.createElement('script');
        script.id = id;
        script.src = this.mapScript;
        script.addEventListener('load', function () {
          _this.initMap();
        });
        document.head.appendChild(script);
      } else {
        this.initMap();
      }
    }
  }, {
    key: "initMap",
    value: function initMap() {
      var _this2 = this;

      this.destroyMap();
      return ymaps.ready().then(function () {
        try {
          var map = new ymaps.Map(_this2.map, {
            center: _this2.center,
            zoom: _this2.zoom,
            controls: []
          });

          if (_this2.isMobile()) {
            map.behaviors.disable('drag');
          }

          map.behaviors.disable('scrollZoom');

          var placemark = _this2.getPlaceMark();

          map.geoObjects.add(placemark);
          map.setBounds(placemark.getBounds()).then(function () {
            if (map.getZoom() > _this2.zoom) map.setZoom(_this2.zoom);
          });

          _this2.initBalloonTemplate();

          map.controls.add(_this2.initZoomControl(), _this2.controlOptions);
          _this2.mapObject = map;
        } catch (e) {
          console.error(e);
        }
      });
    }
  }, {
    key: "initBalloonTemplate",
    value: function initBalloonTemplate() {
      var balloonOptions = {
        balloonContentLayoutClass: ymaps.templateLayoutFactory.createClass("<div class=\"map-balloon-content\">\n                    <b>{{properties.balloonText}}</b>\n                </div>"),
        balloonLayoutClass: ymaps.templateLayoutFactory.createClass('<div class="map-balloon">{% include custom#balloonLayoutContent %}</div>')
      };
      ymaps.layout.storage.add('custom#balloonLayoutContent', balloonOptions.balloonContentLayoutClass);
      ymaps.layout.storage.add('custom#balloonLayout', balloonOptions.balloonLayoutClass);
    }
  }, {
    key: "getPlaceMark",
    value: function getPlaceMark() {
      var _this3 = this;

      try {
        // Создадим коллекцию геообъектов.
        var collection = new ymaps.GeoObjectCollection(null, {
          hasBalloon: true
        });
        console.log(this.items); // Добавляем метки с городами

        this.items.forEach(function (item) {
          collection.add(_this3.createPlaceMark([item.geoX, item.geoY], item.hint));
        });
        return collection;
      } catch (e) {
        console.error(e);
      }
    }
  }, {
    key: "createPlaceMark",
    value: function createPlaceMark() {
      var location = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var hint = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var text = hint ? hint : this.hint;
      return new ymaps.Placemark(location, {
        balloonText: text,
        hintContent: text
      }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: this.placemark,
        // Размеры метки.
        iconImageSize: this.imageSize,
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [-42, -48],
        balloonContentLayout: 'custom#balloonLayoutContent',
        balloonCloseButton: true,
        balloonMaxWidth: 255,
        balloonMapAutoPan: true,
        balloonOffset: [-1, -48],
        hideIconOnBalloonOpen: false
      });
    }
  }, {
    key: "initZoomControl",
    value: function initZoomControl() {
      var self = this; // Создадим пользовательский макет ползунка масштаба.

      var ZoomLayout = ymaps.templateLayoutFactory.createClass(this.zoomTemplate, {
        // Переопределяем методы макета, чтобы выполнять дополнительные действия
        // при построении и очистке макета.
        build: function build() {
          // Вызываем родительский метод build.
          ZoomLayout.superclass.build.call(this); // Привязываем функции-обработчики к контексту и сохраняем ссылки
          // на них, чтобы потом отписаться от событий.

          this.zoomInCallback = ymaps.util.bind(this.zoomIn, this);
          this.zoomOutCallback = ymaps.util.bind(this.zoomOut, this); // Начинаем слушать клики на кнопках макета.

          document.getElementById(self.idZoomIn).addEventListener('click', this.zoomInCallback);
          document.getElementById(self.idZoomOut).addEventListener('click', this.zoomOutCallback);
        },
        clear: function clear() {
          // Снимаем обработчики кликов.
          var zoomIn = document.getElementById(self.idZoomIn);

          if (zoomIn) {
            zoomIn.removeEventListener('click', this.zoomInCallback);
          }

          var zoomOut = document.getElementById(self.idZoomOut);

          if (zoomOut) {
            zoomOut.removeEventListener('click', this.zoomOutCallback);
          } // Вызываем родительский метод clear.


          ZoomLayout.superclass.clear.call(this);
        },
        zoomIn: function zoomIn() {
          var map = this.getData().control.getMap();
          map.setZoom(map.getZoom() + 1, {
            checkZoomRange: true
          });
        },
        zoomOut: function zoomOut() {
          var map = this.getData().control.getMap();
          map.setZoom(map.getZoom() - 1, {
            checkZoomRange: true
          });
        }
      });
      return new ymaps.control.ZoomControl({
        options: {
          layout: ZoomLayout
        }
      });
    }
  }, {
    key: "mapScript",
    get: function get() {
      return this.data.get('script') || 'https://api-maps.yandex.ru/2.1/?lang=ru_RU';
    }
  }, {
    key: "idZoomIn",
    get: function get() {
      return 'zoom-in';
    }
  }, {
    key: "idZoomOut",
    get: function get() {
      return 'zoom-out';
    }
  }, {
    key: "map",
    get: function get() {
      return this.ymapTarget;
    }
  }, {
    key: "zoom",
    get: function get() {
      return this.data.get('zoom') || 14;
    }
  }, {
    key: "center",
    get: function get() {
      return JSON.parse(this.data.get('center')) || [55.755814, 37.617635]; // Москва
    }
  }, {
    key: "items",
    get: function get() {
      var _this4 = this;

      var items = this.itemTargets.map(function (i) {
        return _this4.itemData(i);
      });

      if (0 === items.length) {
        new Error('Не обнаружены данные для карты');
      }

      return items;
    }
  }, {
    key: "placemark",
    get: function get() {
      return this.data.get('placemark') || '/img/map/placemark.svg';
    }
  }, {
    key: "controlOptions",
    get: function get() {
      return {
        position: {
          top: this.isMobile() ? 15 : 20,
          right: this.isMobile() ? 15 : 20
        }
      };
    }
  }, {
    key: "zoomTemplate",
    get: function get() {
      return '<div class="map-zoom">' + "<div id='".concat(this.idZoomIn, "' class='plus'></div>") + "<div id='".concat(this.idZoomOut, "' class='minus'></div>") + '</div>';
    }
  }, {
    key: "imageSize",
    get: function get() {
      return JSON.parse(this.data.get('placemarkSize')) || [48, 48];
    }
  }]);

  return _default;
}(stimulus__WEBPACK_IMPORTED_MODULE_6__["Controller"]);

_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default()(_default, "targets", ['ymap', 'item']);



/***/ }),

/***/ "./src/js/controllers/modal_controller.js":
/*!************************************************!*\
  !*** ./src/js/controllers/modal_controller.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _default; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
/* harmony import */ var _classes_Modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! %classes%/Modal */ "./src/js/classes/Modal.js");






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }




var _default = /*#__PURE__*/function (_Controller) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(_default, _Controller);

  var _super = _createSuper(_default);

  function _default() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, _default);

    return _super.apply(this, arguments);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(_default, [{
    key: "initialize",
    value: function initialize() {
      this.modal = new _classes_Modal__WEBPACK_IMPORTED_MODULE_6__["default"]();
    }
  }, {
    key: "connect",
    value: function connect() {
      this.modal.init();
    }
  }, {
    key: "disconnect",
    value: function disconnect() {
      this.modal.closeAll();
    }
  }]);

  return _default;
}(stimulus__WEBPACK_IMPORTED_MODULE_5__["Controller"]);



/***/ }),

/***/ "./src/js/controllers/share_controller.js":
/*!************************************************!*\
  !*** ./src/js/controllers/share_controller.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _default; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
/* harmony import */ var vanilla_sharing__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vanilla-sharing */ "./node_modules/vanilla-sharing/dist/vanilla-sharing.esm.js");






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }



/**
 * Параметры:
 * url - ссылка на страницу, по умолчанию текущая
 * title - заголовок страницы, по умолчанию текущий
 * description - meta description страницы, по умолчанию текущий
 * img - ссылка на картинку, по умолчанию - нет
 */

var _default = /*#__PURE__*/function (_Controller) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(_default, _Controller);

  var _super = _createSuper(_default);

  function _default() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, _default);

    return _super.apply(this, arguments);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(_default, [{
    key: "getMetaDescription",
    value: function getMetaDescription() {
      var metas = document.getElementsByTagName('meta');

      for (var i in metas) {
        if (typeof metas[i].name != 'undefined' && metas[i].name.toLowerCase() === 'description') {
          return encodeURIComponent(metas[i].content);
        }
      }

      return '';
    }
  }, {
    key: "start",
    value: function start(e) {
      e.preventDefault();
      var el = e.target;
      /* eslint-disable */

      switch (el.dataset.type) {
        case 'vk':
          Object(vanilla_sharing__WEBPACK_IMPORTED_MODULE_6__["vk"])({
            url: this.url,
            title: this.title,
            description: this.description,
            image: this.image
          });
          break;

        case 'fb':
          Object(vanilla_sharing__WEBPACK_IMPORTED_MODULE_6__["fbButton"])({
            url: this.url
          });
          break;

        case 'ok':
          Object(vanilla_sharing__WEBPACK_IMPORTED_MODULE_6__["ok"])({
            url: this.url,
            title: this.title,
            image: this.image
          });
          break;

        case 'whatsapp':
          Object(vanilla_sharing__WEBPACK_IMPORTED_MODULE_6__["whatsapp"])({
            url: this.url,
            title: this.title
          });
          break;

        default:
          console.error('[Error!] Share controller unknown type of share!');
      }
      /* eslint-enable */

    }
  }, {
    key: "type",
    get: function get() {
      return this.data.get('type');
    }
  }, {
    key: "url",
    get: function get() {
      return this.data.get('url') || location.href;
    }
  }, {
    key: "title",
    get: function get() {
      return this.data.get('title') || document.title;
    }
  }, {
    key: "description",
    get: function get() {
      return this.data.get('description') || this.getMetaDescription();
    }
  }, {
    key: "image",
    get: function get() {
      return this.data.get('image');
    }
  }]);

  return _default;
}(stimulus__WEBPACK_IMPORTED_MODULE_5__["Controller"]);



/***/ }),

/***/ "./src/js/controllers/slider-cases_controller.js":
/*!*******************************************************!*\
  !*** ./src/js/controllers/slider-cases_controller.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _default; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! swiper */ "./node_modules/swiper/swiper.esm.js");






function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }




var _default = /*#__PURE__*/function (_Controller) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(_default, _Controller);

  var _super = _createSuper(_default);

  function _default() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, _default);

    return _super.apply(this, arguments);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(_default, [{
    key: "connect",
    value: function connect() {
      new swiper__WEBPACK_IMPORTED_MODULE_6__["default"](this.element, {
        slidesPerView: 'auto',
        grabCursor: true
      });
    }
  }]);

  return _default;
}(stimulus__WEBPACK_IMPORTED_MODULE_5__["Controller"]);



/***/ }),

/***/ "./src/js/controllers/slider-logistic_controller.js":
/*!**********************************************************!*\
  !*** ./src/js/controllers/slider-logistic_controller.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _default; });
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! swiper */ "./node_modules/swiper/swiper.esm.js");







function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }



swiper__WEBPACK_IMPORTED_MODULE_7__["default"].use([swiper__WEBPACK_IMPORTED_MODULE_7__["Navigation"], swiper__WEBPACK_IMPORTED_MODULE_7__["Pagination"]]);

var _default = /*#__PURE__*/function (_Controller) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(_default, _Controller);

  var _super = _createSuper(_default);

  function _default() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, _default);

    return _super.apply(this, arguments);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(_default, [{
    key: "connect",
    value: function connect() {
      new swiper__WEBPACK_IMPORTED_MODULE_7__["default"](this.element, {
        autoplay: {
          delay: 5000,
          disableOnInteraction: true
        },
        slidesPerView: 1,
        loop: true,
        pagination: {
          el: '.swiper-pagination',
          clickable: true
        },
        navigation: {
          nextEl: this.nextTarget,
          prevEl: this.prevTarget
        }
      });
    }
  }]);

  return _default;
}(stimulus__WEBPACK_IMPORTED_MODULE_6__["Controller"]);

_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_5___default()(_default, "targets", ['prev', 'next']);



/***/ }),

/***/ "./src/js/import/application.js":
/*!**************************************!*\
  !*** ./src/js/import/application.js ***!
  \**************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
/* harmony import */ var stimulus_webpack_helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! stimulus/webpack-helpers */ "./node_modules/stimulus/webpack-helpers.js");
/* harmony import */ var turbolinks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! turbolinks */ "./node_modules/turbolinks/dist/turbolinks.js");
/* harmony import */ var turbolinks__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(turbolinks__WEBPACK_IMPORTED_MODULE_2__);



var application = stimulus__WEBPACK_IMPORTED_MODULE_0__["Application"].start();

var context = __webpack_require__("./src/js/controllers sync recursive \\.js$");

application.load(Object(stimulus_webpack_helpers__WEBPACK_IMPORTED_MODULE_1__["definitionsFromContext"])(context));
turbolinks__WEBPACK_IMPORTED_MODULE_2___default.a.start();

/***/ }),

/***/ "./src/js/import/components.js":
/*!*************************************!*\
  !*** ./src/js/import/components.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./src/js/import/modules.js":
/*!**********************************!*\
  !*** ./src/js/import/modules.js ***!
  \**********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_header_header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! %modules%/header/header */ "./src/blocks/modules/header/header.js");
/* harmony import */ var _modules_header_header__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_modules_header_header__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _modules_footer_footer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! %modules%/footer/footer */ "./src/blocks/modules/footer/footer.js");
/* harmony import */ var _modules_footer_footer__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_modules_footer_footer__WEBPACK_IMPORTED_MODULE_1__);



/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _import_application__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./import/application */ "./src/js/import/application.js");
/* harmony import */ var _import_modules__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./import/modules */ "./src/js/import/modules.js");
/* harmony import */ var _import_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./import/components */ "./src/js/import/components.js");
/* harmony import */ var _import_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_import_components__WEBPACK_IMPORTED_MODULE_2__);




/***/ })

/******/ });
//# sourceMappingURL=main.js.map