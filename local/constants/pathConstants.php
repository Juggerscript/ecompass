<?

define('LOCAL_PATH', $_SERVER['DOCUMENT_ROOT'] . '/local/');
define('INCLUDE_PATH', LOCAL_PATH . 'include/');
define('TEMPLATES_PATH', INCLUDE_PATH . 'templates/');
define('COMMON_TEMPLATES_PATH', TEMPLATES_PATH . 'common/');

define('DIST_PATH', LOCAL_PATH . 'assets/dist/');
define('DIST_SITE_DIR_PATH', SITE_DIR . 'local/assets/dist/');

define ('MARKETPLACE_CATALOG_PATH', SITE_DIR . 'marketplace-catalog/');