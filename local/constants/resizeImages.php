<?

/** Централизованное хранилище для ресайзинга картинок, вызываемых CFileResizeImageGet */

return [
    'NEWS_LIST_PICTURE' => [
        'WIDTH' => 376,
        'HEIGHT' => 220,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL,
    ],
    'NEWS_DETAIL_PICTURE' => [
        'WIDTH' => 1000,
        'HEIGHT' => 440,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL,
    ],
    'MARKETPLACE_CATALOG_CARDS_IN_FOOTER' => [
        'WIDTH' => 175,
        'HEIGHT' => 80,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL,
    ],
    'PARTNERS_CARDS_ON_LIST' => [
        'WIDTH' => 130,
        'HEIGHT' => 100,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL,
    ],
    'MARKETPLACE_CATALOG_CARDS_IN_LIST' => [
        'WIDTH' => 131,
        'HEIGHT' => 80,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL,
    ],
    'MARKETPLACE_CATALOG_SPECIALIZATIONS_IN_LIST' => [
        'WIDTH' => 20,
        'HEIGHT' => 20,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL,
    ],
    'MARKETPLACE_CATALOG_CARDS_IN_DETAIL' => [
        'WIDTH' => 175,
        'HEIGHT' => 80,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL,
    ],
    'ABOUT_TEXT_PREVIEW_PICTURE' => [
        'WIDTH' => 1280,
        'HEIGHT' => 612,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL,
    ],
    'YANDEX_MAP_PLACEMARKER' => [
        'WIDTH' => 90,
        'HEIGHT' => 90,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL,
    ],
    'INDEX_REASON_TO_CONNECT' => [
        'WIDTH' => 56,
        'HEIGHT' => 56,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL
    ],
    'INDEX_CASES_IN_LIST' => [
        'WIDTH' => 186,
        'HEIGHT' => 150,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL
    ],
    'INDEX_CASES_IN_LIST_FULL' => [
        'WIDTH' => 656,
        'HEIGHT' => 360,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL
    ],
    'INDEX_PREVIEW' => [
        'WIDTH' => 510,
        'HEIGHT' => 496,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL
    ],
    'INDEX_UNIT-PLATFORM' => [
        'WIDTH' => 1294,
        'HEIGHT' => 470,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL
    ],
    'PEOPLE_IN_CHARGE_LIST' => [
        'WIDTH' => 160,
        'HEIGHT' => 160,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL
    ],
    'SERVICES_LOGISTICS_WINDOW_DETAIL' => [
        'WIDTH' => 925,
        'HEIGHT' => 540,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL
    ],
    'INDEX_SERVICES_LIST' => [
        'WIDTH' => 435,
        'HEIGHT' => 562,
        'BX_RESIZE' => BX_RESIZE_IMAGE_PROPORTIONAL
    ],
];
