<?
/** Infoblocks */
define('IB_NEWS_ID', 1);
define('IB_MARKETPLACE_CATALOG_ID', 2);
define('IB_CAPABILITIES_IBLOCK_ID', 5);
define('IB_CAPABILITIES_ID', 23);
define('IB_SERVICES', 6);
define('FBS_CENTER_ID', 24);
define('B2BASKET_ID', 30);
define('DALYTICS_ID', 31);
define('IB_ANALYTICS_ID', 8);

define('IB_PEOPLE_IN_CHARGE_ID', 7);

/** Highload */
define('HL_TAGS_ID', 2);
define('HL_PARTNERS_ID', 5);
define('HL_MARKETPLACE_SPECIALIZATION_ID', 4);
define('HL_MARKETPLACE_ADVANTAGES_ID', 6);
define('HL_REASONS_TO_CONNECT_ID', 7);
define('HL_CASES_ID', 8);
define('HL_CAPABILITIES_ID', 9);
define('HL_SERVICES_DIFFICULTIES_ID', 10);
define('HL_SERVICE_MARKETPLACE_PLACEMENT_ID', 11);
define('HL_SERVICE_OWN_PLACEMENT_ID', 12);
define('HL_SERVICE_FBS_CENTER_PLACEMENT_ID', 13);
define('HL_SERVICE_KEY_POINTS_ID', 14);
define('HL_SERVICE_ANALYTICS_ID', 17);
define('HL_SERVICE_ANALYTICS_RATES_ID', 16);

/** Элементы текстовых страниц */
define('TEXT_ABOUT_ELEMENT', 16);
define('IB_TEXT', 3);

/** Настройки сайта */
define('IB_SETTINGS', 4);
define('IB_SETTINGS_PHONE_EMAIL', 19);
define('IB_SETTINGS_COORDINATES', 20);
define('IB_SETTINGS_ADDRESS', 18);
define('IB_SETTINGS_MAP_SCRIPT', 21);
define('IB_INDEX_PREVIEW_ELEMENT', 22);