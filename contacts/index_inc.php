<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Letsrock\Lib\Models\SiteSettings;
use Letsrock\Lib\Models\Helper;
Loader::includeModule('iblock');

$element = CIBlockElement::GetByID(TEXT_ABOUT_ELEMENT)->Fetch();
$elementPreviewText = $element['PREVIEW_TEXT'];
$elementDetailText = $element['DETAIL_TEXT'];

$sitePhoneAndEmail = SiteSettings::getPhoneAndNumber();
$sitePhoneNumber = SiteSettings::getPreviewText($sitePhoneAndEmail);
$siteEmail = SiteSettings::getDetailText($sitePhoneAndEmail);
$sitePhoneAndEmailName = SiteSettings::getName($sitePhoneAndEmail);;

$siteAddress = SiteSettings::getAddress();
$siteAddressValue = SiteSettings::getPreviewText($siteAddress);
$siteAddressName = SiteSettings::getName($siteAddress);

$siteCoords = SiteSettings::getCoords();
$siteCoordsPlaceMarkerValue =  SiteSettings::getPreviewText($siteCoords);
$siteAddressCoordsArray = explode(',', $siteCoordsPlaceMarkerValue);
$siteCoordsPlaceMarkerSize = SiteSettings::getDetailText($siteCoords);
$siteMapPreviewPicture = SiteSettings::getPreviewPicture($siteCoords);
$siteMapPreviewPictureResized = Helper::getResizeImageArray($siteMapPreviewPicture, 'YANDEX_MAP_PLACEMARKER');


$siteMapUrl = SiteSettings::getYandexMapPath();
$iteMapUrlValues = SiteSettings::getPreviewText($siteMapUrl);
?>
<section class="contacts"
         data-controller="map"
         data-map-placemark="<?= $siteMapPreviewPictureResized; ?>"
         data-map-placemark-size="<?= $siteCoordsPlaceMarkerSize; ?>" data-map-zoom="12"
         data-map-script="<?= $iteMapUrlValues; ?>">
    <div class="container">
        <div class="contacts__data-list">
            <? if (!empty($siteAddressName) && !empty($siteAddressValue)): ?>
                <article class="contacts__data-item" data-map-target="item"
                         data-hint="<?= $siteAddressValue; ?>"
                         data-geo-x="<?= $siteAddressCoordsArray[0]; ?>"
                         data-geo-y="<?= $siteAddressCoordsArray[1]; ?>">
                    <div class="contacts__data-title"><?= $siteAddressName; ?></div>
                    <div class="contacts__data-body">
                        <div class="contacts__data-address"><?= $siteAddressValue; ?></div>
                    </div>
                </article>
            <? endif; ?>

            <? if (!empty($siteEmail) || !empty($sitePhoneNumber)): ?>
                <article class="contacts__data-item">
                    <? if (!empty($sitePhoneAndEmailName)): ?>
                        <div class="contacts__data-title"><?= $sitePhoneAndEmailName; ?></div>
                    <? endif; ?>
                    <div class="contacts__data-body">
                        <? if (!empty($sitePhoneNumber)): ?>
                            <div class="contacts__data-block">
                                <a class="phone contacts__data-phone" href="tel:<?= $sitePhoneNumber; ?>"><?= $sitePhoneNumber; ?></a>
                            </div>
                        <? endif; ?>
                        <? if (!empty($sitePhoneAndEmail)): ?>
                            <div class="contacts__data-block">
                                <a class="contacts__data-email" href="mailto:<?= $siteEmail; ?>"><?= $siteEmail; ?></a>
                            </div>
                        <? endif; ?>
                    </div>
                </article>
            <? endif; ?>
            <article class="contacts__data-item">
                <div class="contacts__data-title">Социальные сети</div>
                <div class="contacts__data-body">
                    <div class="contacts__data-social-list">
                        <a class="contacts__data-social" href="<?= SiteSettings::getFacebookLink(); ?>" target="_blank">Facebook</a>
                        <a class="contacts__data-social" href="<?= SiteSettings::getTelegramLink(); ?>" target="_blank">Telegram</a>
                        <?/*?>
                        <a class="contacts__data-social" href="https://www.instagram.com/" target="_blank">Instagram</a>
                        <?*/?>
                    </div>
                </div>
            </article>
        </div>
    </div>
    <div class="contacts__content">
        <div class="map">
            <div class="map__inner" id="map-contacts" data-map-target="ymap"></div>
        </div>
</section>
