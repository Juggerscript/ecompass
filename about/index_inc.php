<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
Loader::includeModule('iblock');
use Letsrock\Lib\Models\Helper;

$element = CIBlockElement::GetByID(TEXT_ABOUT_ELEMENT)->Fetch();
$elementPreviewText = $element['PREVIEW_TEXT'];
$elementDetailText = $element['DETAIL_TEXT'];
if ($element) {
    $elementPicture = $element['PREVIEW_PICTURE'];
    if (!empty($elementPicture)) {
        $elementPictureResized = Helper::getResizeImageArray($elementPicture, 'ABOUT_TEXT_PREVIEW_PICTURE');
    }
}
?>
<? if (!empty($element)): ?>
    <section class="about">
        <div class="container">
            <div class="about__cover">
                <? if (!empty($elementPictureResized)): ?>
                    <img class="about__cover-img" src="<?= $elementPictureResized; ?>" alt="">
                <? endif; ?>
            </div>
            <div class="about__body">
                <? if (!empty($elementPreviewText) || !empty($elementPreviewText)): ?>
                    <div class="about__content text">
                        <?= $elementPreviewText; ?>
                        <?= $elementDetailText; ?>
                    </div>
                <? endif; ?>
            </div>
        </div>
    </section>
<? endif; ?>
